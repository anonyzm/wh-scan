#!/bin/bash
if [ -f "./app/.env" ]; then
  rm -rf ./app/.env
fi
cp "./app/.env.$1" ./app/.env
echo ".env created"

if [ -h "./.env" ]; then
  rm -rf ./.env
fi
ln -s ./app/.env ./.env
echo "symlink created"