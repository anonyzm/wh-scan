#!/bin/bash
if [ ! -f "./app/.env" ]; then
  cp "./app/.env.$1" ./app/.env
  echo ".env created"
fi
if [ ! -h "./.env" ]; then
  ln -s ./app/.env ./.env
  echo "symlink created"
fi
docker-compose up -d
docker-compose exec --user application php composer -d/app update
if [ ! -h "./app/vendor/bower" ]; then
  docker-compose exec --user application php ln -s /app/vendor/bower-asset /app/vendor/bower
  echo "bower fixed created"
fi
docker-compose exec --user application php /app/yii app/init