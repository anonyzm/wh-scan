<?php

namespace app\controllers;

use app\models\Anomaly;
use app\models\StaticWH;
use app\models\System;
use app\modules\comments\models\Comment;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class SitemapController extends Controller
{
    public function actionIndex()
    {
        header("Content-Type: text/xml");
        $urls = [];
        $systems = System::find()->all();
        foreach ($systems as $system) {
            /** @var System $system */
            /** @var Comment $comment */
            $comment = Comment::find()->where(['item_type' => 'system', 'item_id' => $system->id])->orderBy(['created_at' => SORT_DESC])->one();
            $lastmod = $comment ? date('Y-m-d', $comment->created_at) : '2020-11-20';
            $urls[] = [
                'loc' => 'https://whscan.info/system/' . $system->title,
                'lastmod' => $lastmod,
            ];
        }
        $anomalies = Anomaly::find()->all();
        foreach ($anomalies as $anomaly) {
            /** @var Anomaly $anomaly */
            /** @var Comment $comment */
            $comment = Comment::find()->where(['item_type' => 'anomaly', 'item_id' => $anomaly->id])->orderBy(['created_at' => SORT_DESC])->one();
            $lastmod = $comment ? date('Y-m-d', $comment->created_at) : '2020-11-20';
            $urls[] = [
                'loc' => 'https://whscan.info/anomaly/' . $anomaly->id,
                'lastmod' => $lastmod,
            ];
        }
        $statics = StaticWH::find()->all();
        foreach ($statics as $static) {
            /** @var StaticWH $static */
            $comment = Comment::find()->where(['item_type' => 'static', 'item_id' => $static->id])->orderBy(['created_at' => SORT_DESC])->one();
            $urls[] = [
                'loc' => 'https://whscan.info/static/' . $static->title,
                'lastmod' => '2020-11-20',
            ];
        }
        echo '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            <url>
                <loc>https://whscan.info/</loc>
                <lastmod>2020-11-20-</lastmod>
                <changefreq>monthly</changefreq>
            </url>
            <url>
                <loc>https://whscan.info/notifications/notification/index</loc>
                <lastmod>2020-11-20-</lastmod>
                <changefreq>monthly</changefreq>
            </url>';
        foreach ($urls as $url) {
            echo "<url>
                <loc>{$url['loc']}</loc>
                <lastmod>{$url['lastmod']}</lastmod>
                <changefreq>monthly</changefreq>
            </url>";
        }
        echo '</urlset>';

        die();
    }
}
