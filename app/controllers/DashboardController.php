<?php

namespace app\controllers;

use app\models\Anomaly;
use app\models\EveType;
use app\models\forms\SystemSearch;
use app\models\Planet;
use app\models\StaticWH;
use app\models\System;
use app\models\ZKill;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DashboardController extends Controller
{
    public function actionIndex()
    {
        $now = time();
        $kills = ZKill::find()
            ->joinWith(['system'])
            ->where(['>', 'time', ($now - 86400 * 7)])
            ->orderBy('`time` DESC')->all();
        $weeklyChart = [];
        $datesWeek = [];
        for($i = ($now - 86400 * 7); $i <= $now; $i += 3600) {
            $dtWeek = date('d M', $i);
            $datesWeek[$dtWeek] = $dtWeek;
            $weeklyChart[$dtWeek] = 0;
        }

        foreach($kills as $kill) {
            /** @var $kill ZKill */
            $dtWeek = date('d M', $kill->time);
            $weeklyChart[$dtWeek] += 1;
        }

        $randomSystem = System::find()->orderBy('RAND()')->limit(1)->one();
        $randomSystems = System::find()->orderBy('RAND()')->limit(5)->all();

        return $this->render('index', [
            'weeklyChart' => array_values($weeklyChart),
            'datesWeek' => array_values($datesWeek),
            'randomSystem' => $randomSystem,
            'randomSystems' => $randomSystems,
        ]);
    }

    public function actionSystem($title) {
        $system = System::getByName($title);
        if(!$system) {
            throw new NotFoundHttpException('Specified system not found!');
        }
        /** @var $system System */

        $anomalyList = [];
        foreach($system->anomalies as $anomaly) {
            $anomalyList[$anomaly->type][] = $anomaly;
        }

        $now = time();
        $kills = ZKill::find()
            ->joinWith(['system'])
            ->where(['system_id' => $system->external_id])
            ->andWhere(['>', 'time', ($now - 86400 * 7)])
            ->orderBy('`time` DESC')->all();
        $weeklyChart = [];
        $dailyChart = [];
        $datesWeek = [];
        $datesDay = [];
        for ($i = ($now - 86400 * 7); $i <= $now; $i += 3600) {
            $dtWeek = date('d M', $i);
            $dtDay = date('d.m.y H:00', $i);
            $datesWeek[$dtWeek] = $dtWeek;
            $datesDay[$dtDay] = $dtDay;
            $weeklyChart[$dtWeek] = 0;
            $dailyChart[$dtDay] = 0;
        }

        foreach ($kills as $kill) {
            /** @var $kill ZKill */
            $dtWeek = date('d M', $kill->time);
            $dtDay = date('d.m.y H:00', $kill->time);

            $weeklyChart[$dtWeek] += 1;
            $dailyChart[$dtDay] += 1;
        }

        $latestKills = ZKill::find()
            ->where(['system_id' => $system->external_id])
            ->orderBy('`time` DESC')
            ->andWhere(['>', 'time', ($now - 86400 * 30)])
            ->limit(10)
            ->all();

        $typeIds = [];
        foreach ($latestKills as $kill) {
            $typeIds[] = $kill->victim->ship_type_id;
            $typeIds[] = $kill->attacker->ship_type_id;
        }
        $shipTypes = EveType::find()->where(['id' => $typeIds])->indexBy('id')->all();

        $planetInfo = [];
        $planets = ArrayHelper::getValue($system->getCcpObject(), 'planets', []);
        foreach ($planets as $p) {
            $planet = null;
            $attempt = 0;
            do {
                try {
                    $planet = Planet::findOne($p['planet_id']);
                } catch (\Throwable $e) {
                    Yii::error($e);
                    usleep(300);
                }
                $attempt++;
            } while (!($planet instanceof Planet) && $attempt < 3);

            if ($planet instanceof Planet) {
                $planetInfo[] = $planet;
            }
        }

        return $this->render('system', [
            'system' => $system,
            'anomalyList' => $anomalyList,
            'planets' => $planetInfo,
            // EVE kills chart
            'weeklyChart' => array_values($weeklyChart),
            'dailyChart' => array_values($dailyChart),
            'datesWeek' => array_values($datesWeek),
            'datesDay' => array_values($datesDay),
            'latestKills' => $latestKills,
            'killsShipTypes' => $shipTypes,
        ]);
    }

    public function actionAutocomplete()
    {
        if (!\Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('Bad request');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $term = \Yii::$app->request->get('term', '');
        return \app\models\System::find()
            ->select(['`title` AS value', '`title` AS label'])
            ->where(['LIKE', 'title', $term])
            ->limit(15)
            ->asArray()
            ->all();
    }

    public function actionAnomaly($id)
    {
        $anomaly = Anomaly::findOne($id);
        if(!$anomaly) {
            throw new NotFoundHttpException('Anomaly type not found');
        }

        return $this->render('anomaly', [
            'anomaly' => $anomaly,
        ]);
    }

    public function actionStatic($title)
    {
        $static = StaticWH::find()->where(['title' => $title])->one();
        if(!$static) {
            throw new NotFoundHttpException('Static type not found');
        }

        return $this->render('static', [
            'static' => $static,
        ]);
    }

    public function actionSearch()
    {
        if(\Yii::$app->request->isPost) {
            $model = new SystemSearch();
            if($model->load(\Yii::$app->request->post())) {
                if($model->validate()) {
                    $system = $model->getSystem();
                    $this->redirect('/system/'.$system->title);
                }
            }
        }
        else {
            throw new BadRequestHttpException('Bad request');
        }
    }

}
