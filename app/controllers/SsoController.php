<?php

namespace app\controllers;

use app\components\EveSso;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class SsoController extends \yii\web\Controller
{
    public function actionCallback()
    {
        $code = \Yii::$app->request->get('code');

        $response = \Yii::$app->evesso->verifyCode($code);
        $token = ArrayHelper::getValue($response, 'access_token');
        if(!$token) {
            \Yii::$app->session->addFlash('error', 'Unknown error occured during authentification!');
            \Yii::error('verifyCode ERROR: '.json_encode($response));
        }
        else {
            $cookies = \Yii::$app->response->cookies;
            $ssoUser = \Yii::$app->evesso->login($response);
            if(!$ssoUser) {
                \Yii::$app->session->addFlash('error', 'Unknown error occured during login!');
                \Yii::error('login ERROR: '.json_encode($ssoUser));
            }
            else {
                $cookies->add(new \yii\web\Cookie([
                    'name' => EveSso::COOKIE_PARAM_NAME,
                    'value' => $ssoUser->cookieAuthToken,
                    'expire' => time() + 86400 * 30,
                ]));
            }
        }

        $this->redirect('/');
    }

    public function actionLogin($scope = '')
    {
        $this->redirect(\Yii::$app->evesso->getLoginUrl($scope));
    }
}