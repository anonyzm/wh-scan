<?php

namespace app\controllers\backend;

use Yii;
use app\models\Anomaly;
use app\models\search\AnomalySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Базовый класс контроллеров бэкенда с проверкой прав
 * Class BaseBackendController
 * @package app\controllers\backend
 */
class BaseBackendController extends Controller
{
    public function beforeAction($action)
    {
        if(!\Yii::$app->evesso->isAdmin()) {
            $this->redirect('/');
        }

        return parent::beforeAction($action);
    }
}
