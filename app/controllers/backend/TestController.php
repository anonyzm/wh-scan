<?php

namespace app\controllers\backend;

use app\models\EveType;
use Yii;

/**
 * ТЕстовый контроллер бэкенда
 * Class TestController
 * @package app\controllers\backend
 */
class TestController extends BaseBackendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

}
