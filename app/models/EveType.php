<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%eve_types}}".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $title
 * @property string $description
 */
class EveType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eve_types}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }
}
