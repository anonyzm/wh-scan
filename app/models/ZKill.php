<?php

namespace app\models;

/**
 * This is the model class for table "zkillboard_kills".
 *
 * @property int $id
 * @property int $killmail_id
 * @property int $system_id
 * @property int $npc
 * @property int $solo
 * @property int $awox
 * @property int $totalValue
 * @property int $time
 * @property System $system
 * @property ZVictim $victim
 * @property ZAttacker[] $attackers
 * @property ZAttacker $attacker
 */
class ZKill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zkillboard_kills';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['killmail_id', 'system_id', 'time'], 'required'],
            [['killmail_id', 'system_id', 'npc', 'solo', 'awox', 'totalValue', 'time'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'killmail_id' => 'Killmail ID',
            'system_id' => 'System ID',
            'npc' => 'Npc',
            'solo' => 'Solo',
            'awox' => 'Awox',
            'totalValue' => 'Total Value',
            'time' => 'Time',
        ];
    }

    public function getSystem() {
        return $this->hasOne(System::class, ['external_id' => 'system_id']);
    }

    public function getVictim() {
        return $this->hasOne(ZVictim::class, ['zkill_id' => 'killmail_id']);
    }

    public function getAttackers() {
        return $this->hasMany(ZAttacker::class, ['zkill_id' => 'killmail_id']);
    }

    /**
     * @return ZAttacker|null
     */
    public function getAttacker() {
        $finalBlowAttacker = null;
        foreach ($this->attackers as $attacker) {
            /** @var ZAttacker $attacker */
            if ($attacker->final_blow) {
                $finalBlowAttacker = $attacker;
                break;
            }
        }
        $killer = $finalBlowAttacker;

        if ($finalBlowAttacker->npc) {
            foreach ($this->attackers as $attacker) {
                /** @var ZAttacker $attacker */
                if (!$attacker->npc) {
                    $killer = $attacker;
                    break;
                }
            }
        }

        return $killer;
    }
}
