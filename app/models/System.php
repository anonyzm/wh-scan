<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%system}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property double $security
 * @property integer $effect_id
 * @property integer $class_id
 * @property integer $external_id
 * @property string $CCP_object
 * @property Effect $effect
 * @property Anomaly[] $anomalies
 * @property StaticWH[] $statics
 */
class System extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%system}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'security'], 'required'],
            [['description', 'CCP_object'], 'string'],
            [['security'], 'number'],
            [['effect_id', 'class_id', 'external_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }
    
    public function getEffect() {
        return $this->hasOne(Effect::className(), ['id' => 'effect_id']);
    }

    public function getAnomalies() {
        return $this->hasMany(Anomaly::className(), ['id' => 'anomaly_id'])->viaTable('system_class_anomaly', ['class_id' => 'class_id']);
    }

    public function getStatics() {
        return $this->hasMany(StaticWH::className(), ['id' => 'static_id'])->viaTable('system_static', ['system_id' => 'id']);
    }

    public static function getByName($title) {
        return self::find()->where(['title' => $title])->one();
    }

    public function getCcpObject() {
        return Json::decode($this->CCP_object);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'security' => 'Security',
            'effect_id' => 'Effect ID',
            'class_id' => 'Class ID',
            'external_id' => 'External ID',
            'CCP_object' => 'CCP Object',
        ];
    }
}
