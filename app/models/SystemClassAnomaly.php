<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%system_class_anomaly}}".
 *
 * @property integer $class_id
 * @property integer $anomaly_id
 */
class SystemClassAnomaly extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%system_class_anomaly}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_id', 'anomaly_id'], 'required'],
            [['class_id', 'anomaly_id'], 'integer'],
            [['class_id', 'anomaly_id'], 'unique', 'targetAttribute' => ['class_id', 'anomaly_id'], 'message' => 'The combination of Class ID and Anomaly ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_id' => 'Class ID',
            'anomaly_id' => 'Anomaly ID',
        ];
    }
}
