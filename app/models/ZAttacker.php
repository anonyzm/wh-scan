<?php

namespace app\models;

/**
 * This is the model class for table "zkillboard_attackers".
 *
 * @property int $id
 * @property int $zkill_id
 * @property int $npc
 * @property int $ship_type_id
 * @property int $weapon_type_id
 * @property int $faction_id
 * @property int $character_id
 * @property int $corporation_id
 * @property int $alliance_id
 * @property boolean $final_blow
 * @property string $character_name
 * @property string $character_picture
 * @property string $corporation_name
 * @property string $corporation_picture
 * @property string $alliance_name
 * @property string $alliance_picture
 */
class ZAttacker extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zkillboard_attackers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zkill_id'], 'required'],
            [['zkill_id', 'ship_type_id', 'character_id', 'corporation_id', 'alliance_id', 'faction_id', 'final_blow', 'weapon_type_id'], 'integer'],
            [['character_name', 'character_picture', 'corporation_name', 'corporation_picture', 'alliance_name', 'alliance_picture'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'zkill_id' => 'Zkill ID',
            'npc' => 'NPC',
            'faction_id' => 'Faction ID',
            'final_blow' => 'Final Blow',
            'ship_type_id' => 'Ship Type ID',
            'character_id' => 'Character ID',
            'corporation_id' => 'Corporation ID',
            'alliance_id' => 'Alliance ID',
            'character_name' => 'Character Name',
            'character_picture' => 'Character Picture',
            'corporation_name' => 'Corporation Name',
            'corporation_picture' => 'Corporation Picture',
            'alliance_name' => 'Alliance Name',
            'alliance_picture' => 'Alliance Picture',
        ];
    }
}
