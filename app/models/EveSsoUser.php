<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%eve_sso_user}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $token_type
 * @property string $token
 * @property string $refreshToken
 * @property string $cookieAuthToken
 * @property string $scopes
 * @property array $scopesList
 * @property integer $expires_at
 */
class EveSsoUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eve_sso_user}}';
    }

    public static function findByToken($cookieToken) {
        return self::findOne(['cookieAuthToken' => $cookieToken]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'token', 'refreshToken', 'name'], 'required'],
            [['expires_at'], 'integer'],
            [['name', 'token_type', 'token', 'refreshToken', 'cookieAuthToken', 'scopes'], 'string', 'max' => 255],
        ];
    }

    public function beforeValidate()
    {
        if(empty($this->cookieAuthToken)) {
            $this->cookieAuthToken = $this->generateCookieAuthToken();
        }

        return parent::beforeValidate();
    }

    protected function generateCookieAuthToken() {
        return base64_encode($this->id . md5($this->token));
    }

    public function getAvatar($width = 32) {
        return \Yii::$app->evesso->getCharacterAvatar($this->id, $width);
    }

    public function getScopesList() {
        return Json::decode($this->scopes);
    }

    public function canReadNotifications() {
        return in_array('esi-characters.read_notifications.v1', $this->scopesList);
    }

    public function setScopesList($value) {
        if(!is_array($value)) {
            $value = [$value];
        }
        $this->scopes = Json::encode($value);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'token' => 'Token',
            'cookieAuthToken' => 'Cookie Token',
            'expires_at' => 'Expires At',
        ];
    }
}
