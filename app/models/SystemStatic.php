<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%system_static}}".
 *
 * @property integer $id
 * @property integer $system_id
 * @property integer $static_id
 */
class SystemStatic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%system_static}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['system_id', 'static_id'], 'required'],
            [['system_id', 'static_id'], 'integer'],
            [['system_id', 'static_id'], 'unique', 'targetAttribute' => ['system_id', 'static_id'], 'message' => 'The combination of System ID and Static ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'system_id' => 'System ID',
            'static_id' => 'Static ID',
        ];
    }
}
