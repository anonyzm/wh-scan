<?php

namespace app\models\forms;

use app\models\System;
use yii\base\Model;

class SystemSearch extends Model
{
    public $title;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['title', 'required'],
            ['title', 'string'],
        ];
    }

    /**
     * @return null|System
     */
    public function getSystem()
    {
        return System::getByName($this->title);
    }
}
