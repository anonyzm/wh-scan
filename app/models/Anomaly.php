<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%anomaly}}".
 *
 * @property integer $id
 * @property string $type
 * @property string $title
 * @property string $description
 */
class Anomaly extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%anomaly}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'title'], 'required'],
            [['description'], 'string'],
            [['type', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }
}
