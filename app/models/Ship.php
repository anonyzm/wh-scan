<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%ships}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $graphic_id
 */
class Ship extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ships}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'description', 'graphic_id'], 'required'],
            [['id', 'graphic_id'], 'integer'],
            [['title', 'description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'graphic_id' => 'Graphic ID',
        ];
    }
}
