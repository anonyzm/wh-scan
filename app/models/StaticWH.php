<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%static}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $max_stable_mass
 * @property string $max_stable_time
 * @property string $max_jump_mass
 * @property string $max_mass_regeneration
 */
class StaticWH extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%static}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['title', 'description', 'max_stable_mass', 'max_stable_time', 'max_jump_mass', 'max_mass_regeneration'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'max_stable_mass' => 'Max Stable Mass',
            'max_stable_time' => 'Max Stable Time',
            'max_jump_mass' => 'Max Jump Mass',
            'max_mass_regeneration' => 'Max Mass Regeneration',
        ];
    }
}
