<?php

namespace app\models;

use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%planets}}".
 *
 * @property integer $id
 * @property integer $system_id
 * @property System $system
 * @property integer $type_id
 * @property EveType $type
 * @property string $name
 * @property string $moons
 */
class Planet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%planets}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['system_id', 'type_id'], 'required'],
            [['system_id', 'type_id'], 'integer'],
            [['moons', 'name'], 'string', 'max' => 255],
        ];
    }

    public function getType() {
        return $this->hasOne(EveType::className(), ['id' => 'type_id']);
    }

    public function getSystem() {
        return $this->hasOne(System::className(), ['id' => 'system_id']);
    }

    /**
     * @return mixed
     */
    public function getMoons() {
        $moons = Json::decode($this->moons);
        return is_array($moons) ? $moons : [];
    }

    public static function findOne($condition)
    {
        $planet = parent::findOne($condition);
        if(!$planet) {
            if(is_numeric($condition)) {
                $planet_id = intval($condition);
            }
            else if(is_array($condition) && key_exists('id', $condition)) {
                $planet_id = $condition['id'];
            }
            else {
                return null;
            }

            //$ccpPlanet = \Yii::$app->evesso->planet($planet_id);
            $ccpPlanet = file_get_contents("https://esi.evetech.net/latest/universe/planets/{$planet_id}/?datasource=tranquility&language=en-us");
            $ccpPlanet = Json::decode($ccpPlanet);
            if(!empty($ccpPlanet['planet_id'])) {
                $planet = new self([
                    'id' => $ccpPlanet['planet_id'],
                    'name' => $ccpPlanet['name'],
                    'system_id' => $ccpPlanet['system_id'],
                    'type_id' => $ccpPlanet['type_id'],
                    'moons' => '[]',
                ]);

                $system = System::findOne(['external_id' => $ccpPlanet['system_id']]);
                if ($system) {
                    $ccpSystem = $system->getCcpObject();
                    foreach($ccpSystem['planets'] as $p) {
                        if($p['planet_id'] == $ccpPlanet['planet_id']) {
                            break;
                        }
                    }
                    $planet->moons = Json::encode(ArrayHelper::getValue($p, 'moons', []));
                }

                if(!$planet->validate() || !$planet->save()) {
                    throw new Exception('Error saving planet: '.Json::encode($planet->firstErrors));
                }

            }
        }

        return $planet;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'system_id' => 'System ID',
            'type_id' => 'Type ID',
            'moons' => 'Moons',
        ];
    }
}
