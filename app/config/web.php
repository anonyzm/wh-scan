<?php

$common = require(__DIR__ . '/common.php');

$config = \yii\helpers\ArrayHelper::merge($common, [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['comments', 'notifications'],
    'defaultRoute' => 'dashboard/index',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'qSzsuw09ugiN8yx6_EmMuug_e-tOaOEv',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'system/<title:\w+>' => 'dashboard/system',
                'static/<title:\w+>' => 'dashboard/static',
                'anomaly/<id:\d+>' => 'dashboard/anomaly',
                'backend/anomaly/<id:\d+>' => 'backend/anomaly',
                'sitemap.xml' => 'sitemap/index',
            ],
        ],
    ],
    'modules' => [
        'comments' => [
            'class' => 'app\modules\comments\Module',
        ],
        'notifications' => [
            'class' => 'app\modules\notifications\Module',
        ],
    ],
]);

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => explode(',', str_replace(' ', '', getenv('ALLOWED_DEBUG_IPS'))),
    ];
}

return $config;
