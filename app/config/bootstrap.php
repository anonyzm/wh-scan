<?php
require __DIR__.'/../vendor/symfony/dotenv/Dotenv.php';
$dotenv = new \Symfony\Component\Dotenv\Dotenv();
$dotenv->load(__DIR__.'/../.env');