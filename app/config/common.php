<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'evesso' => [
            'class' => 'app\components\EveSso',
            'appID' => getenv('EVE_APP_ID'),
            'secretKey' => getenv('EVE_APP_SECRET'),
            'callbackUrl' => getenv('EVE_CALLBACK_URL'),
        ],
        'zKillboard' => [
            'class' => \app\components\zKillboard\ZKillboard::class,
        ],
        'cache' => [
            'class' => \yii\redis\Cache::class,
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => [],
                    'logFile' => '/app/runtime/logs/error.log'
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logVars' => [],
                    'logFile' => '/app/runtime/logs/app.log'
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host='.getenv('MYSQL_HOST').';dbname='.getenv('MYSQL_DATABASE'),
            'username' => getenv('MYSQL_USER'),
            'password' => getenv('MYSQL_PASSWORD'),
            'charset' => 'utf8',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => getenv('REDIS_HOST'),
        ],
        'mutex' => [
            'class' => \yii\mutex\MysqlMutex::class,
        ],
    ],
    'params' => $params,
];

return $config;
