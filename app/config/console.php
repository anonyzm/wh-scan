<?php

$common = require(__DIR__ . '/common.php');

$config = \yii\helpers\ArrayHelper::merge($common, [
    'id' => 'basic-console',
    'controllerNamespace' => 'app\console',
    'components' => [

    ],
]);

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
