<?php

return [
    'zKillListen' => getenv('ZKILL_LISTEN') === 'true',
    'siteDomain' => getenv('SITE_URL'),
    'adminEmail' => getenv('ADMIN_EMAIL'),
    'anomalies' => [
        'Anomaly' => 'Cosmic Anomalies',
        'Relic' => 'Relic Sites - Magnetometric',
        'Data' => 'Data Sites - Radar',
        'Gas' => 'Gas Sites - Ladar',
        'Ore' => 'Ore Sites - Gravimetric'
    ],
    'commentsEnabled' => ('true' === getenv('COMMENTS_ENABLED')),
];
