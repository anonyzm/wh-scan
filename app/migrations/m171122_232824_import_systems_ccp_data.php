<?php

use yii\db\Migration;

/**
 * Class m171122_232824_import_systems_ccp_data
 */
class m171122_232824_import_systems_ccp_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        \app\helpers\ToolsHelper::systemLinkIds();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171122_232824_import_systems_ccp_data cannot be reverted.\n";
        
        return false;
    }
}
