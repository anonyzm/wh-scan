<?php

use yii\db\Migration;

/**
 * Class m180216_024752_alter_user_table_add_token_type_field
 */
class m180216_024752_alter_user_table_add_token_type_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('eve_sso_user', 'token_type', $this->string()->after('name'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('eve_sso_user', 'token_type');
    }
}
