<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notifications`.
 */
class m180216_032244_create_notifications_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        /*
        {
            "notification_id":765465823,
            "type":"StructureDestroyed",
            "sender_id":1000137,
            "sender_type":"corporation",
            "timestamp":"2018-02-14T21:18:00Z",
            "is_read":true,
            "text":"ownerCorpLinkData:\n- showinfo\n- 2\n- 98450101\nownerCorpName: Growbrain Inc\nsolarsystemID: 31000221\nstructureID: &id001 1026855966732\nstructureShowInfoData:\n- showinfo\n- 35835\n- *id001\nstructureTypeID: 35835\n"
        },*/
        $this->createTable('notifications', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'sender_id' => $this->integer(),
            'sender_type' => $this->string(),
            'timestamp' => $this->integer(),
            'text' => $this->text(),
        ]);

        $this->createIndex('idx-timestamp', 'notifications', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('notifications');
    }
}
