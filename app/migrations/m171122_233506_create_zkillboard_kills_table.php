<?php

use yii\db\Migration;

/**
 * Handles the creation of table `zkillboard_kills`.
 */
class m171122_233506_create_zkillboard_kills_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('zkillboard_kills', [
            'id' => $this->primaryKey(),
            'killmail_id' => $this->integer()->notNull(),
            'npc' => $this->boolean()->notNull(),
            'fittedValue' => $this->double(),
            'time' => $this->bigInteger()->notNull()
        ]);

        $this->createIndex('idx-killmail_id', 'zkillboard_kills', 'killmail_id', true);
        $this->createIndex('idx-time', 'zkillboard_kills', 'time');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('zkillboard_kills');
    }
}
