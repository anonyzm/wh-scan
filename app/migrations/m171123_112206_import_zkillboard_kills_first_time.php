<?php

use yii\db\Migration;

/**
 * Class m171123_112206_import_zkillboard_kills_first_time
 */
class m171123_112206_import_zkillboard_kills_first_time extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //\app\helpers\ToolsHelper::gatherSystemKills();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('zkillboard_kills');
    }
}
