<?php

use yii\db\Migration;

/**
 * Class m171123_115018_alter_ships_table_add_columns
 */
class m171123_115018_alter_ships_table_add_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('ships', 'title', $this->string(255));
        $this->addColumn('ships', 'description', $this->text());
        $this->addColumn('ships', 'graphic_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('ships', 'title');
        $this->dropColumn('ships', 'description');
        $this->dropColumn('ships', 'graphic_id');
    }
}
