<?php

use app\helpers\ToolsHelper;
use yii\db\Migration;

/**
 * Class m201120_204658_generate_types_css
 */
class m201120_204658_generate_types_css extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        ToolsHelper::generateTypesCss();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201120_204658_generate_types_css cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201120_204658_generate_types_css cannot be reverted.\n";

        return false;
    }
    */
}
