<?php

use yii\db\Migration;

/**
 * Class m171129_221751_alter_comment_table_to_utf8
 */
class m171129_221751_alter_comment_table_to_utf8 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        \Yii::$app->db->createCommand("ALTER TABLE `comment` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;")->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
