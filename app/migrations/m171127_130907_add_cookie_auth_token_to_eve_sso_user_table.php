<?php

use yii\db\Migration;

/**
 * Class m171127_130907_add_cookie_auth_token_to_eve_sso_user_table
 */
class m171127_130907_add_cookie_auth_token_to_eve_sso_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('eve_sso_user', 'cookieAuthToken', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('eve_sso_user', 'cookieAuthToken');
    }
}
