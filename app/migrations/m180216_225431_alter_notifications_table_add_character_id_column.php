<?php

use yii\db\Migration;

/**
 * Class m180216_225431_alter_notifications_table_add_character_id_column
 */
class m180216_225431_alter_notifications_table_add_character_id_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('notifications', 'character_id', $this->integer());
        $this->createIndex('idx-character_id', 'notifications', 'character_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('notifications', 'character_id');
    }

}
