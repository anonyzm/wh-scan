<?php

use yii\db\Migration;

/**
 * Class m171211_001548_import_types
 */
class m171211_001548_import_types extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        \app\helpers\ToolsHelper::importTypes();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('eve_types');
    }
}
