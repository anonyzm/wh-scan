<?php

use yii\db\Migration;

/**
 * Class m171210_234556_add_types_table
 */
class m171210_234556_add_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('eve_types', [
            'id' => $this->primaryKey(),
            'group_id' => $this->integer(),
            'title' => $this->string(255),
            'description' => $this->text(),
        ]);

        $this->createIndex('idx-group_id', 'eve_types', 'group_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('eve_types');
    }

}
