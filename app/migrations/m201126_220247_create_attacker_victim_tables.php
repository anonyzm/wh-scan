<?php

use yii\db\Migration;

/**
 * Class m201126_220247_create_attacker_victim_tables
 */
class m201126_220247_create_attacker_victim_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('zkillboard_kills');

        $this->createTable('zkillboard_kills', [
            'id' => $this->primaryKey(),
            'killmail_id' => $this->integer()->notNull(),
            'system_id' => $this->integer()->notNull(),
            'npc' => $this->boolean()->defaultValue(false),
            'solo' => $this->boolean()->defaultValue(false),
            'awox' => $this->boolean()->defaultValue(false),
            'totalValue' => $this->integer()->defaultValue(0),
            'time' => $this->integer(),
        ]);

        $this->createIndex('idx-killmail_id', 'zkillboard_kills', 'killmail_id');
        $this->createIndex('idx-system_id', 'zkillboard_kills', 'system_id');

        $this->createTable('zkillboard_attackers', [
            'id' => $this->primaryKey(),
            'zkill_id' => $this->integer()->notNull(),
            'ship_type_id' => $this->integer(),
            'character_id' => $this->integer(),
            'corporation_id' => $this->integer(),
            'alliance_id' => $this->integer(),
            'character_name' => $this->string(255),
            'character_picture' => $this->string(255),
            'corporation_name' => $this->string(255),
            'corporation_picture' => $this->string(255),
            'alliance_name' => $this->string(255),
            'alliance_picture' => $this->string(255),
        ]);

        $this->createIndex('idx-zkill_id', 'zkillboard_attackers', 'zkill_id');
        $this->createIndex('idx-ship_type_id', 'zkillboard_attackers', 'ship_type_id');
        $this->createIndex('idx-character_id', 'zkillboard_attackers', 'character_id');
        $this->createIndex('idx-corporation_id', 'zkillboard_attackers', 'corporation_id');
        $this->createIndex('idx-alliance_id', 'zkillboard_attackers', 'alliance_id');

        $this->createTable('zkillboard_victims', [
            'id' => $this->primaryKey(),
            'zkill_id' => $this->integer()->notNull(),
            'ship_type_id' => $this->integer(),
            'character_id' => $this->integer(),
            'corporation_id' => $this->integer(),
            'alliance_id' => $this->integer(),
            'character_name' => $this->string(255),
            'character_picture' => $this->string(255),
            'corporation_name' => $this->string(255),
            'corporation_picture' => $this->string(255),
            'alliance_name' => $this->string(255),
            'alliance_picture' => $this->string(255),
        ]);

        $this->createIndex('idx-zkill_id', 'zkillboard_victims', 'zkill_id');
        $this->createIndex('idx-ship_type_id', 'zkillboard_victims', 'ship_type_id');
        $this->createIndex('idx-character_id', 'zkillboard_victims', 'character_id');
        $this->createIndex('idx-corporation_id', 'zkillboard_victims', 'corporation_id');
        $this->createIndex('idx-alliance_id', 'zkillboard_victims', 'alliance_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
