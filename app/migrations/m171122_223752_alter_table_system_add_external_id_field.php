<?php

use yii\db\Migration;

/**
 * Class m171122_223752_alter_table_system_add_external_id_field
 */
class m171122_223752_alter_table_system_add_external_id_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('system', 'external_id', $this->integer());
        $this->createIndex('idx-external_id', 'system', 'external_id', true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('system', 'external_id');
    }

}
