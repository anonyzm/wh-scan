<?php

use yii\db\Migration;

/**
 * Class m171125_181512_insert_rhea_wh_data
 */
class m171125_181512_insert_rhea_wh_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        \app\helpers\ToolsHelper::importShatteredSystems();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171125_181512_insert_rhea_wh_data cannot be reverted.\n";

        return false;
    }
}
