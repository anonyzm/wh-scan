<?php

use yii\db\Migration;

/**
 * Class m171125_221212_fix_effects_table
 */
class m171125_221212_fix_effects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        \app\helpers\ToolsHelper::fixShatteredSystems();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171125_221212_fix_effects_table cannot be reverted.\n";

        return false;
    }
}
