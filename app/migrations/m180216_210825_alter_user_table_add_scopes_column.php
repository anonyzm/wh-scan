<?php

use yii\db\Migration;

/**
 * Class m180216_210825_alter_user_table_add_scopes_column
 */
class m180216_210825_alter_user_table_add_scopes_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('eve_sso_user', 'scopes', $this->string()->defaultValue('[]'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('eve_sso_user', 'scopes');
    }

}
