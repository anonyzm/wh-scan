<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zkillboard_attackers}}`.
 */
class m201129_100727_add_final_blow_column_to_zkillboard_attackers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('zkillboard_attackers', 'final_blow', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('zkillboard_attackers', 'final_blow');
    }
}
