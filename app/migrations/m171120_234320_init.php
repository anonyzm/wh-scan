<?php

use yii\db\Migration;

/**
 * Class m171120_234320_init
 */
class m171120_234320_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // anomalies
        $this->createTable('anomaly', [
            'id' => $this->primaryKey(),
            'type' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text(),
        ]);

        // junction table system -> anomalies
        $this->createTable('system_class_anomaly', [
            'class_id' => $this->integer()->notNull(),
            'anomaly_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-class-anomaly', 'system_class_anomaly', ['class_id', 'anomaly_id'], true);

        // systems
        $this->createTable('system', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'security' => $this->float()->notNull(),
            'effect_id' => $this->integer()->null(),
            'class_id' => $this->integer()->null(),
        ]);

        $this->createIndex('idx-effect_id', 'system', 'effect_id');
        $this->createIndex('idx-class_id', 'system', 'class_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('anomaly');
        $this->dropTable('system_class_anomaly');
        $this->dropTable('system');
    }

}
