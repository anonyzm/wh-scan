<?php

use yii\db\Migration;

/**
 * Class m171128_000803_add_updated_at_field_to_comment_table
 */
class m171128_000803_add_updated_at_field_to_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('comment', 'updated_at', $this->integer()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('comment', 'updated_at');
    }
}
