<?php

use yii\db\Migration;

/**
 * Handles the creation of table `effect`.
 */
class m171121_092659_create_effect_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        // effects
        $this->createTable('effect', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text(),
        ]);

        $this->insert('effect', [
            'id' => 1,
            'title' => 'Pulsar',
            'description' => '<img src="/images/eve/icon01_13.png"> <font color="green">Shield +58%</font><br><img src="/images/eve/icon01_03.png"><img src="/images/eve/icon01_01.png"> <font color="green">Nos &amp; Neut Drain Amount +58%</font><br><img src="/images/eve/icon03_09.png"> <font color="green">Signature +58%</font><br><img src="/images/eve/icon01_09.png"> <font color="red">Armor Resistances -29%</font><br><img src="/images/eve/icon12_04.png"> <font color="red">Cap recharge -29%</font>',
        ]);

        $this->insert('effect', [
            'id' => 2,
            'title' => '',
            'description' => '<img src="/images/eve/icon22_13.png"> <font color="green">Ship velocity +30%</font><br><img src="/images/eve/icon22_24.png"> <font color="green">Inertia +15%</font><br><img src="/images/eve/icon22_11.png"> <font color="green">Missile Explosion Velocity +30%</font><br><img src="/images/eve/icon22_13.png"> <font color="green">Missile velocity +15%</font><br><img src="/images/eve/icon22_15.png"> <font color="green">Lock Range +30%</font><br><img src="/images/eve/icon12_06.png"> <font color="red">Stasis Webifier Strength -15%</font>',
        ]);

        $this->insert('effect', [
            'id' => 3,
            'title' => 'Cataclysmic Variable',
            'description' => '<img src="/images/eve/icon01_11.png"> <font color="red">Repair amount -36%</font><br><img src="/images/eve/icon02_01.png"> <font color="red">Shield Boost amount -36%</font><br><img src="/images/eve/icon01_02.png"> <font color="red">Remote Capacitor Transmitter amount -36%</font><br><img src="/images/eve/icon01_16.png"> <font color="green">Shield transfer +72%</font><br><img src="/images/eve/icon01_11.png"> <font color="green">Remote repair +72%</font><br><img src="/images/eve/icon01_01.png"> <font color="green">Capacitor capacity +72%</font><br><img src="/images/eve/icon01_01.png"> <font color="green">Capacitor recharge +36%</font>',
        ]);

        $this->insert('effect', [
            'id' => 4,
            'title' => 'Magnetar',
            'description' => '<img src="/images/eve/icon50_15.png"> <font color="green">Damage +58%</font><br><img src="/images/eve/icon22_11.png"> <font color="green">Missile Explosion Radius +29%</font><br><img src="/images/eve/icon56_06.png"> <font color="red">Drone Tracking -29%</font><br><img src="/images/eve/icon22_15.png"> <font color="red">Targeting Range -29%</font><br><img src="/images/eve/icon22_14.png"> <font color="red">Tracking Speed -29%</font><br><img src="/images/eve/icon56_01.png"> <font color="red">Target Painter Strength -29%</font>',
        ]);

        $this->insert('effect', [
            'id' => 5,
            'title' => 'Wolf Rayet',
            'description' => '<img src="/images/eve/icon01_09.png"> <font color="green">Armor Hit Points +86%</font><br><img src="/images/eve/icon02_04.png"> <font color="red">Shield Resistances -43%</font><br><img src="/images/eve/icon13_09.png"> <font color="green">Small Weapon Damage +172%</font><br><img src="/images/eve/icon03_09.png"> <font color="red">Signature Size -43%</font>
',
        ]);

        $this->insert('effect', [
            'id' => 6,
            'title' => 'Red Giant',
            'description' => '<img src="/images/eve/icon22_10.png"> <font color="green">Heat Damage +15%</font><br><img src="/images/eve/icon31_02.png"> <font color="green">Overload Bonus +30%</font><br><img src="/images/eve/icon22_15.png"> <font color="green">Smart Bomb Range +30%</font><br><img src="/images/eve/icon03_04.png"> <font color="green">Smart Bomb Damage +30%</font><br><img src="/images/eve/icon70_07.png"> <font color="green">Bomb Damage +30%</font>',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('effect');
    }
}
