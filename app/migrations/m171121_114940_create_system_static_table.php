<?php

use yii\db\Migration;

/**
 * Handles the creation of table `system_static`.
 */
class m171121_114940_create_system_static_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('system_static', [
            'id' => $this->primaryKey(),
            'system_id' => $this->integer()->notNull(),
            'static_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-system-static', 'system_static', ['system_id', 'static_id'], true);
        $this->createIndex('idx-system_id', 'system_static', 'system_id');
        $this->createIndex('idx-static_id', 'system_static', 'static_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('system_static');
    }
}
