<?php

use yii\db\Migration;

/**
 * Handles adding type to table `comment`.
 */
class m171130_225608_add_type_column_to_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->truncateTable('comment');
        $this->dropIndex('idx-system_id', 'comment');
        $this->renameColumn('comment', 'system_id', 'item_id');
        $this->addColumn('comment', 'item_type', $this->string(255)->defaultValue('default'));
        $this->createIndex('idx-item_id', 'comment', 'item_id');
        $this->createIndex('idx-item_type', 'comment', 'item_type');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}