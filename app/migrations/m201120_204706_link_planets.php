<?php

use app\helpers\ToolsHelper;
use yii\db\Migration;

/**
 * Class m201120_204706_link_planets
 */
class m201120_204706_link_planets extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        ToolsHelper::linkPlanets();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201120_204706_link_planets cannot be reverted.\n";

        return false;
    }
}
