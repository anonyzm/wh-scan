<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zkillboard_attackers}}`.
 */
class m201129_203725_add_weapon_type_id_column_to_zkillboard_attackers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('zkillboard_attackers', 'weapon_type_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('zkillboard_attackers', 'weapon_type_id', $this->integer());
    }
}
