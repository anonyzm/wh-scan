<?php

use yii\db\Migration;

/**
 * Class m171124_114228_fix_effects_data
 */
class m171124_114228_fix_effects_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        echo "Fixing data for effect #2".PHP_EOL;
        $effect = \app\models\Effect::findOne(2);
        $effect->title = 'Black hole';
        $effect->save();
        echo json_encode($effect->getAttributes()).PHP_EOL;
        echo "DONE!".PHP_EOL;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171124_114228_fix_effects_data cannot be reverted.\n";

        return false;
    }
}
