<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ships`.
 */
class m171122_232924_create_ships_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ships', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('ships');
    }
}
