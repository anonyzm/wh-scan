<?php

use yii\db\Migration;

/**
 * Class m201129_130310_add_npc_faction_id_fields_to_zkillboard_attackers_table
 */
class m201129_130310_add_npc_faction_id_fields_to_zkillboard_attackers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('zkillboard_attackers', 'npc', $this->boolean()->defaultValue(false));
        $this->addColumn('zkillboard_attackers', 'faction_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('zkillboard_attackers', 'npc');
        $this->dropColumn('zkillboard_attackers', 'faction_id');
    }
}
