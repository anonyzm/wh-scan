<?php

use yii\db\Migration;

/**
 * Handles the creation of table `static`.
 */
class m171121_092733_create_static_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('static', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->string(255)->notNull(),
            'max_stable_mass' => $this->string(255),
            'max_stable_time' => $this->string(255),
            'max_jump_mass' => $this->string(255),
            'max_mass_regeneration' => $this->string(255),
        ]);

        $this->createIndex('idx-title', 'static', 'title', true);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('static');
    }
}
