<?php

use yii\db\Migration;

/**
 * Class m180218_012702_alter_table_planets_add_name_column
 */
class m180218_012702_alter_table_planets_add_name_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('planets', 'name', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('planets', 'name');
    }
}
