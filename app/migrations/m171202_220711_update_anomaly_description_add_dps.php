<?php

use yii\db\Migration;

/**
 * Class m171202_220711_update_anomaly_description_add_dps
 */
class m171202_220711_update_anomaly_description_add_dps extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $sql = file_get_contents(__DIR__.'/files/anomaly_dps.sql');
        \Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171202_220711_update_anomaly_description_add_dps cannot be reverted.\n";

        return false;
    }

}
