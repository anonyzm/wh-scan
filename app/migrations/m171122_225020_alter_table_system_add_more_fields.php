<?php

use yii\db\Migration;

/**
 * Class m171122_225020_alter_table_system_add_more_fields
 */
class m171122_225020_alter_table_system_add_more_fields extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('system', 'CCP_object', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('system', 'CCP_object');
    }
}
