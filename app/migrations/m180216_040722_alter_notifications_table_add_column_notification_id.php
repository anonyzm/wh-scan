<?php

use yii\db\Migration;

/**
 * Class m180216_040722_alter_notifications_table_add_column_notification_id
 */
class m180216_040722_alter_notifications_table_add_column_notification_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('notifications', 'notification_id', $this->integer()->notNull()->after('id'));
        $this->createIndex('idx-notification_id', 'notifications', 'notification_id', true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('notifications', 'notification_id');
    }
}
