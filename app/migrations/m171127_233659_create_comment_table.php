<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m171127_233659_create_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'system_id' => $this->integer()->notNull(),
            'character_id' => $this->integer()->notNull(),
            'text' => $this->text(),
            'created_at' =>$this->integer(),
        ]);

        $this->createIndex('idx-system_id', 'comment', 'system_id');
        $this->createIndex('idx-character_id', 'comment', 'character_id');
        $this->createIndex('idx-created_at', 'comment', 'created_at');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('comment');
    }
}
