<?php

use yii\db\Migration;

/**
 * Handles the creation of table `planets`.
 */
class m180218_010600_create_planets_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('planets', [
            'id' => $this->primaryKey(),
            'system_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'moons' => $this->string()->defaultValue('[]'),
        ]);

        $this->createIndex('idx-system_id', 'planets', 'system_id');
        $this->createIndex('idx-type_id', 'planets', 'type_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('planets');
    }
}
