<?php

use yii\db\Migration;

/**
 * Class m171122_124240_fix_anomaly_data
 */
class m171122_124240_fix_anomaly_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $sql = "UPDATE `anomaly` SET `description` = REPLACE(`description`, '\"images/', '\"/images/eve/') WHERE INSTR(`description`, 'images/') > 0;";
        echo "Patching `anomaly` data...".PHP_EOL;
        \Yii::$app->db->createCommand($sql)->execute();
        echo "Done!".PHP_EOL;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171122_124240_fix_anomaly_data cannot be reverted.\n";

        return false;
    }
}
