<?php

use yii\db\Migration;

/**
 * Handles the creation of table `eve_sso_user`.
 */
class m171127_130429_create_eve_sso_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('eve_sso_user', [
            'id' => $this->primaryKey(), // EVE characte rID
            'name' => $this->string(255), // EVE character Name
            'token' => $this->string(255), // EVE SSO access token
            'expires_at' => $this->integer(),
        ]);

        $this->createIndex('idx-created_at', 'eve_sso_user', 'expires_at');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('eve_sso_user');
    }
}
