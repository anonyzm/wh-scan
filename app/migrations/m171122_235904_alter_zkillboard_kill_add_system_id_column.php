<?php

use yii\db\Migration;

/**
 * Class m171122_235904_alter_zkillboard_kill_add_system_id_column
 */
class m171122_235904_alter_zkillboard_kill_add_system_id_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('zkillboard_kills', 'system_id', $this->integer());
        $this->createIndex('idx-system_id', 'zkillboard_kills', 'system_id');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('zkillboard_kills', 'system_id');
    }
}
