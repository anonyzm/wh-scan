<?php

use yii\db\Migration;

/**
 * Class m180216_014608_alter_user_table
 */
class m180216_014608_alter_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('eve_sso_user', 'refreshToken', $this->string()->after('token'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('eve_sso_user', 'refreshToken');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180216_014608_alter_user_table cannot be reverted.\n";

        return false;
    }
    */
}
