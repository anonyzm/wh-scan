<?php

namespace app\console;

use yii\console\Controller;

class AppController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     */
    public function actionInit()
    {
        $this->run('migrate/up', ['interactive' => false]);
    }
}
