<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\console;

use app\helpers\ToolsHelper;
use app\models\EveType;
use app\models\ZKill;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ToolsController extends Controller
{
    public function actionWhList() {
        ToolsHelper::whList();
    }

    public function actionWhLink() {
        ToolsHelper::whLink();
    }

    public function actionSystemLinkIds($start = 31000000) {
        ToolsHelper::systemLinkIds($start);
    }

    public function actionGatherKills($pastSeconds = null, $debug = 0) {
        ToolsHelper::gatherSystemKills($pastSeconds, $debug);
        //sleep(1);
        //ToolsHelper::syncKillShips();
    }

    public function actionSyncShips() {
        ToolsHelper::syncKillShips();
    }

    public function actionImportShattered() {
        ToolsHelper::importShatteredSystems();
    }

    public function actionFixShattered() {
        ToolsHelper::fixShatteredSystems();
    }

    public function actionImportTypes() {
        ToolsHelper::importTypes();
    }

    public function actionGenerateTypesCss() {
        ToolsHelper::generateTypesCss();
    }

    public function actionLinkPlanets() {
        ToolsHelper::linkPlanets();
    }

    public function actionLatestKills() {
        $kills = ZKill::find()->with('system')->limit(5)->all();
        foreach ($kills as $kill) {
            /** @var ZKill $kill */
            Console::output($kill->system->title);
        }
    }

    public function actionEveTypes() {
        Console::output(EveType::find()->count());
    }

    public function actionEveType($type_id) {
        var_dump(EveType::find()->where(['id' => $type_id])->asArray()->one());
        echo PHP_EOL;
    }

    public function actionDb($table, $field, $value) {
        var_dump(Yii::$app->db->createCommand("SELECT * FROM {$table} WHERE {$field} = '{$value}'")->queryAll());
        echo PHP_EOL;
    }
}
