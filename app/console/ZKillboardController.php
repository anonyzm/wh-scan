<?php
namespace app\console;

use app\components\ZKillConsister;
use app\models\System;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

class ZKillboardController extends Controller
{
    const MUTEX_NAME = 'ZKillboardController.actionListen_LOCK';
    const WH_SYSTEMS_CACHE_KEY = 'wh_systems_ids';

    /** @var ZKillConsister */
    protected $consister;

    public function init()
    {
        parent::init();
        $this->consister = new ZKillConsister();
    }

    public function actionListen()
    {
        // Если надо - выполняем, если нет - скипаем
        if (!Yii::$app->params['zKillListen']) {
            Console::output('ZKill listen skipped');
            return;
        }
        if (!Yii::$app->mutex->acquire(self::MUTEX_NAME, 1800)) {
            Console::output('MutexLocked');
            Yii::error('ZKillboardController.actionListen lock cannot be aquired!');
        }
        $start = microtime(true);

        while (true) {
            $kill = Yii::$app->zKillboard->listen();
            if (is_null($kill)) {
                Console::output('No kills');
                break;
            }
            if (!$this->isWH($kill->killmail->solar_system_id)) {
                Console::output('Not WH kill, skipping...');
                continue;
            }

            // собрабатываем киллмейл
            $zkill = $this->consister->consistZKill($kill);
            Console::output('Kill #' . $zkill->id . ' saved');
        }

        Console::output('Done in ' . (microtime(true) - $start) . ' seconds');
        Yii::$app->mutex->release(self::MUTEX_NAME);
    }

    /**
     * Получаем список айдишников ВХ систем
     * @param $system_id
     * @return bool
     */
    protected function isWH($system_id) {
        $whIds = Yii::$app->cache->getOrSet(self::WH_SYSTEMS_CACHE_KEY, function () {
            return Yii::$app->db->createCommand('SELECT `external_id` FROM ' . System::tableName())->queryAll(\PDO::FETCH_COLUMN, 0);
        }, 3600);

        return in_array($system_id, $whIds);
    }
}