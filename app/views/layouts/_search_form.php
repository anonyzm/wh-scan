<?php

/* @var $this \yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;

$model = new \app\models\forms\SystemSearch();
$form = ActiveForm::begin([
    'action' => ['/dashboard/search'],
    'options' => [
        'class' => 'system-search-form',
        'enctype' => 'multipart/form-data',
    ],
    'fieldConfig' => [
        'options' => [
            //'tag' => false,
            'showErrors' => false,
            'showHints' => false,
        ],
        'template' => "{input}",
    ],
    'enableClientValidation' => false,
]) ?>
    <div class="input-wrap">
        <span>System:</span><?= $form->field($model, 'title')->widget(
            AutoComplete::className(), [
            'clientOptions' => [
                'source' => \yii\helpers\Url::to('/dashboard/autocomplete'),
                'select' => new \yii\web\JsExpression('function( event, ui ) {
                    $(".autocomplete-system-search").val(ui.item.label);
                    $(".system-search-form").submit();
                    return false;
                }'),
            ],
            'options' => [
                'class' => 'autocomplete-system-search'
            ]
        ]);
        ?><!--input type="submit" value="Go"-->
    </div>
<?php ActiveForm::end(); ?>
<?php
    $js = "
        $('.autocomplete-system-search').keyup(function () {
            var offset = $(this).offset();
            $('ul.ui-autocomplete').css('top', (offset.top + 18) + 'px');
            $('ul.ui-autocomplete').css('left', offset.left + 'px');
        });
    ";
    $this->registerJs($js);
?>
