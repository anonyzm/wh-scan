<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EveType */

$this->title = 'Update Eve Type: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Eve Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="eve-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
