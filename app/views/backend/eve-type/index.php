<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\EveTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Eve Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eve-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Eve Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            'description:ntext',
            [
                'attribute' => 'Icon',
                'format' => 'raw',
                'value' => function (\app\models\EveType $type) {
                    return Html::tag('span', '', ['class' => "eve-type-big eve-type-{$type->id}"]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
