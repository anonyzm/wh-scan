<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EveType */

$this->title = 'Create Eve Type';
$this->params['breadcrumbs'][] = ['label' => 'Eve Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eve-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
