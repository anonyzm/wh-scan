<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Anomaly */

$this->title = 'Create Anomaly';
$this->params['breadcrumbs'][] = ['label' => 'Anomalies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anomaly-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
