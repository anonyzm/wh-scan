<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\comments\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'item_id')->hiddenInput()->label(false) ?>

    <?php //= $form->field($model, 'character_id')->textInput() ?>

    <h4><?= $model->character->name ?></h4>
    <p>&nbsp;</p>

    <?php //= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
    <?= \mervick\emojionearea\Widget::widget([
        'model' => $model,
        'attribute' => 'text',
        'name' => 'Comment[text]',
        'pluginOptions' => [
            'saveEmojisAs' => 'shortname',
        ],
        'options' => [],
    ]); ?>

    <?php //= $form->field($model, 'item_type')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
