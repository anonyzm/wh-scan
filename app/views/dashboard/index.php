<?php

/* @var $this yii\web\View */
/* @var $weeklyChart array */
/* @var $datesWeek array */
/* @var $randomSystem \app\models\System */
/* @var $randomSystems \app\models\System[] */

$this->title = 'Wormhole Scanner';
?>
<div class="row">
    <div class="col-sm-7 anomaly-wrapper">
        <h1>&gt; Welcome</h1>
        <p><b>Wormhole Scanner</b> is a simple tool for EVE Online™, that can help you surfing W-space.</p>
        <p>Out database contains all information you may need during your exciting journey through wormholes.</p>
        <ul>
            <li>Basic information for each system (name, class, effect, statics)</li>
            <li>Static detail view (max mass, max time, max jump mass, etc.) </li>
            <li>Anomaly list for each system grouped by type</li>
            <li>Anomaly detalisation (waves, triggers, effects)</li>
            <li>Weekly player kills statistic for each system</li>
        </ul>
        <h3>Latest news</h3>
        <p>
            <b>2020/12/17</b>
        </p>
        <ul>
            <li>New ZKillboard integration.</li>
            <li>Finally you can check latest kills in system view.</li>
        </ul>
        <p>
            <b>2018/02/17</b>
        </p>
        <ul>
            <li>Planetary information released</li>
            <li>New block added to system view - <b>Planets</b>. Now you don't need to be online anymore to check what kind of planets are located in system you are insterested in.</li>
            <li>Basic planet information provided: planet name, planet type, moons count.</li>
        </ul>
        <p>
            <b>2018/02/16</b>
        </p>
        <ul>
            <li>EVE notifications in your browser NOW!</li>
            <li>Someone is attacking you citadel or one of your POSes have insufficient fuel? Don't worry, you will be up to date with any EVE notifications in your browser.</li>
            <li>Simply login using Login button on <?= \yii\bootstrap\Html::a('this page', 'notifications/notification/index') ?>, allow browser notifications, and you will receive EVE notifications within your browser.</li>

        </ul>
        <p>
            <b>2017/12/03</b>
        </p>
        <ul>
            <li>DPS info added for NPC waves in anomalies</li>
            <li>You can comment every single W-system and every anomaly. Just sign in using EVE SSO login and share your experience with other players</li>
            <li>We DO NOT request any scopes. EVE login is required only to identify you character. The only things, that are requested is character name and ID. Be sure your privacy is safe - <b>NO scopes are requested</b>.</li>
        </ul>
        <p>
            <b>2017/11/26</b>
        </p>
        <ul>
            <li>Fonts fixed</li>
            <li>Shattered systems added</li>
            <li>Icons fixed for system effects</li>
            <li>Many small bugs fixed</li>
        </ul>
        <p>&nbsp;</p>
        <p>
            Random W-systems:
            <?php foreach ($randomSystems as $key=>$system) : ?>
                <?= \yii\bootstrap\Html::a($system->title, ["system/{$system->title}"], ['class' => 'system-title']) ?><?php if ( ($key + 1) < count($randomSystems)) echo ', '; ?>
            <?php endforeach; ?>
        </p>
        <h3>Official threads</h3>
        <ul>
            <li><a href="https://forums.eveonline.com/t/whscan-info-wormhole-database-and-some-more/281063" target="_blank">EVE Online Forums</a></li>
            <li><a href="http://forum.eve-ru.com/index.php?showtopic=118336" target="_blank">EVE-RU Forums</a></li>
        </ul>
        <p>Feel free to donate ISK to <b>Ano Woodaxe</b></p>
    </div>
    <div class="col-sm-5">
        <div class="system-info start-info">
            <h3 class="system-name">Start with WH Scanner</h3>
            <p class="start-text">
                To start using <b>Wormhole Scanner</b> type system name (ex. <?= \yii\bootstrap\Html::a($randomSystem->title, "/system/{$randomSystem->title}", ['class' => 'system-title']) ?>) in text input above.
                <?= \yii\bootstrap\Html::img('/images/arrow_up.png', ['class' => 'arrow-up']) ?>
            </p>
        </div>
        <div class="system-info margin-top">
            <h3 class="system-name">Browser notifications (beta)</h3>
            <?php if(\Yii::$app->evesso->isLoggedIn() && \Yii::$app->evesso->user->canReadNotifications()) : ?>
                <h4 class="system-label">Status: <span style="color:green">ON</span></h4>
            <?php else: ?>
                <h4 class="system-label">Status: <span style="color:red">OFF</span></h4>
                <p><?= \yii\bootstrap\Html::a('Login', ["notifications/notification/index"], ['class' => 'system-title']) ?> to enable browser notifications</p>
            <?php endif; ?>
        </div>
        <div class="system-info margin-top">
            <?= $this->render('_kill_chart', [
                'datesWeek' => $datesWeek,
                'weeklyChart' => $weeklyChart,
            ]); ?>
        </div>
    </div>
</div>