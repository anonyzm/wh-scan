<?php
/* @var $this yii\web\View */
/* @var $system \app\models\System */
/* @var $anomalyList array */
/* @var $weeklyChart array */
/* @var $dailyChart array */
/* @var $datesWeek array */
/* @var $datesDay array */
/* @var $planets \app\models\Planet[] */
/* @var $latestKills \app\models\ZKill[] */
/* @var $killsShipTypes \app\models\EveType */

use yii\helpers\ArrayHelper;

$anomaliesTypesCombat = [
    'Anomaly' => 'Cosmic Anomalies',
    'Relic' => 'Relic Sites - Magnetometric',
    'Data' => 'Data Sites - Radar',
];
$anomaliesTypesGather = [
    'Gas' => 'Gas Sites - Ladar',
    'Ore' => 'Ore Sites - Gravimetric'
];
$this->title = "{$system->title} | Wormhole Scanner";
//$anomaliesTypes = \yii\helpers\ArrayHelper::getValue(\Yii::$app->params, 'anomalies');
?>
<div class="row margin-bottom-large">
    <div class="col-md-7 anomaly-wrapper">
        <h1>W-system <span class="grey-text"><?= $system->title ?></span></h1>
        <div class="row anomalies">
            <div class="col-md-6">
                <?php foreach ($anomaliesTypesCombat as $type=>$typeTitle) : ?>
                    <h4><?= $typeTitle ?></h4>
                    <ul class="anomaly-list">
                        <?php foreach (\yii\helpers\ArrayHelper::getValue($anomalyList, $type, []) as $anomaly) : ?>
                            <?php /* @var $anomaly \app\models\Anomaly */ ?>
                            <li>
                                <?= \yii\bootstrap\Html::a($anomaly->title, "/anomaly/{$anomaly->id}") ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                <?php endforeach ?>
            </div>
            <div class="col-md-6">
                <?php foreach ($anomaliesTypesGather as $type=>$typeTitle) : ?>
                    <h4><?= $typeTitle ?></h4>
                    <ul class="anomaly-list">
                        <?php foreach (\yii\helpers\ArrayHelper::getValue($anomalyList, $type, []) as $anomaly) : ?>
                            <?php /* @var $anomaly \app\models\Anomaly */ ?>
                            <li>
                                <?= \yii\bootstrap\Html::a($anomaly->title, "/anomaly/{$anomaly->id}") ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                <?php endforeach ?>
            </div>
        </div>
        <h2>Latest kills</h2>
        <div class="col-md-12 row">
            <table class="table">
                <thead>
                    <th colspan="4" class="kills-victim">Victim</th>
                    <th colspan="3" class="kills-attacker">Attacker</th>
                </thead>
                <tbody>
                    <?php if(!count($latestKills)) : ?>
                        <tr class="system-kills-table-empty">
                            <td colspan="7">
                                No recent kills found.
                            </td>
                        </tr>
                    <?php else : ?>
                        <?php foreach ($latestKills as $kill) : ?>
                            <?php
                                /** @var \app\models\ZKill $kill */
                                $attackerShip = ArrayHelper::getValue($killsShipTypes, $kill->attacker->ship_type_id . '.title', '');
                                $victimShip = ArrayHelper::getValue($killsShipTypes, $kill->victim->ship_type_id . '.title', '');
                                $attackerTicker = $kill->attacker->alliance_id ? $kill->attacker->alliance_name : $kill->attacker->corporation_name;
                                $victimTicker = $kill->victim->alliance_id ? $kill->victim->alliance_name : $kill->victim->corporation_name;
                                $attackerTickerPicture = $kill->attacker->alliance_id ? $kill->attacker->alliance_picture : $kill->attacker->corporation_picture;
                                $victimTickerPicture = $kill->victim->alliance_id ? $kill->victim->alliance_picture : $kill->victim->corporation_picture;
                            ?>
                            <tr>
                                <td style="min-width: 54px">
                                    <?= date('M j', $kill->time) ?><br>
                                    <?= date('H:i', $kill->time) ?>
                                </td>
                                <td>
                                    <img src="<?= Yii::$app->evesso->getTypeIcon($kill->victim->ship_type_id) ?>"
                                         title="<?= $victimShip ?>"
                                         alt="<?= $victimShip ?>"
                                         class="type-image" />
                                </td>
                                <td>
                                    <img src="<?= $victimTickerPicture ?>"
                                         title="<?= $victimTicker ?>"
                                         alt="<?= $victimTicker ?>"
                                         class="type-image" />
                                </td>
                                <td>
                                    <b><?= $kill->victim->character_name ?></b>&nbsp;(<?= $victimShip ?>)<br>
                                    <i><?= $victimTicker ?></i>
                                </td>
                                <td>
                                    <img src="<?= Yii::$app->evesso->getTypeIcon($kill->attacker->ship_type_id) ?>"
                                         title="<?= $attackerShip ?>"
                                         alt="<?= $attackerShip ?>"
                                         class="type-image" />
                                </td>
                                <td>
                                    <img src="<?= $attackerTickerPicture ?>"
                                         title="<?= $attackerTicker ?>"
                                         alt="<?= $attackerTicker ?>"
                                         class="type-image" />
                                </td>
                                <td>
                                    <?php if ($kill->npc) : ?>
                                        <?= $attackerShip ?>
                                    <?php else : ?>
                                        <b><?= $kill->attacker->character_name ?></b>&nbsp;(<?= $attackerShip ?>)<br>
                                        <i><?= $attackerTicker ?></i>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-5">
        <div class="system-info class-<?= $system->class_id ?>">
            <h3 class="system-name"><?= $system->title ?></h3>
            <h4><span class="system-label">Class:</span> <?= $system->class_id ?? '?' ?></h4>
            <?php if (!empty($system->effect)) : ?>
                <h4 class="system-label">Effect: <span class="grey-text"><?= $system->effect->title ?></span></h4>
                <p class="system-effect-description"><?= $system->effect->description ?></p>
            <?php endif ?>
            <?php if (!empty($system->description)) : ?>
                <h4 class="system-label">Information:</h4>
                <p style="color:red"><?= $system->description ?></p>
            <?php endif ?>
            <?php if(!empty($system->statics)) : ?>
                <h4 class="statics-h system-label">Statics:</h4>
                <?php foreach ($system->statics as $static) : ?>
                    <p>
                        <?= \yii\bootstrap\Html::a($static->title, "/static/{$static->title}", ['class' => 'static-link']) ?><br />
                        <i><?= $static->description ?></i>
                    </p>
                <?php endforeach ?>
            <?php endif ?>
        </div>
        <div class="system-info margin-top">
            <?= $this->render('_kill_chart', [
                'datesWeek' => $datesWeek,
                'weeklyChart' => $weeklyChart,
            ]); ?>
        </div>
        <div class="system-info margin-top margin-bottom">
            <?= $this->render('_system_planets', [
                'planets' => $planets,
            ]); ?>
        </div>
    </div>
</div>
<?php if(\yii\helpers\ArrayHelper::getValue(\Yii::$app->params, 'commentsEnabled', false)) : ?>
<div class="row">
    <div class="col-sm-12">
        <?= \app\modules\comments\widgets\CommentWidget::widget([
            'title' => 'Comments',
            'item_id' => $system->id,
            'item_type' => 'system',
        ]) ?>
    </div>
</div>
<?php endif ?>