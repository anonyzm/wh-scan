<?php
/**
 * @var $datesWeek array
 * @var $weeklyChart array
 */
?>
<h3>Player kills <span class="grey-text"><?=reset($datesWeek) ?> - <?=end($datesWeek) ?></span></h3>
<?php
echo \miloschuman\highcharts\Highcharts::widget([
    'options' => [
        'title' => null,['text' => 'Weekly kills'],
        'xAxis' => [
            'categories' => $datesWeek,
        ],
        'yAxis' => [
            'title' => null,
            'allowDecimals' => false,
        ],
        'series' => [
            ['name' => 'Player kills', 'data' => $weeklyChart],
        ],
        'chart' => [
            'height' => '250',
            'marginTop' => '20',
            'backgroundColor' => 'transparent',
            'events' => [
                'load' => new \yii\web\JsExpression('function (e) {
                    $("text:contains(\'Highcharts.com\')").hide();
                }'),
            ],
        ],
        'legend' => [
            'enabled' => false,
        ],
    ]
]);
?>