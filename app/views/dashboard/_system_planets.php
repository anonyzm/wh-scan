<?php
/**
 * @var $planets \app\models\Planet[]
 */
?>
<h3>Planets</h3>
<ul class="system-planets">
    <?php foreach($planets as $planet) : ?>
        <?php /** @var \app\models\Planet $planet */ ?>
        <li class="eve-type-<?=$planet->type_id ?>">
            <?= $planet->name ?><br />
            <?= $planet->type->title ?><br />
            Moons: <?= count($planet->getMoons()) ?>
        </li>
    <?php endforeach; ?>
</ul>