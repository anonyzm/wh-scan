<?php
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $anomaly \app\models\Anomaly */

$this->title = "{$anomaly->title} | Wormhole Scanner";
$anomaliesTypes = \yii\helpers\ArrayHelper::getValue(\Yii::$app->params, 'anomalies');
?>
<div class="row margin-bottom-large">
    <div class="col-md-7 anomaly-wrapper">
        <h1><?= $anomaly->title ?></h1>
        <div class="text-info">
            <?= $anomaly->description ?>
        </div>
    </div>
    <div class="col-md-5">
        <div class="system-info margin-bottom">
            <h3 class="system-name"><?= $anomaly->title ?></h3>
            <h4 class="system-label">Type:</h4>
            <p><?= \yii\helpers\ArrayHelper::getValue($anomaliesTypes, $anomaly->type, '') ?></p>
        </div>
        <?php if(\Yii::$app->evesso->isAdmin()) : ?>
            <div class="system-info">
                <h4>Edit</h4>
                <?= Html::a('Edit anomaly', ["/backend/anomaly/update", 'id' => $anomaly->id]) ?>
            </div>
        <?php endif ?>
    </div>
</div>
<?php if(\yii\helpers\ArrayHelper::getValue(\Yii::$app->params, 'commentsEnabled', false)) : ?>
<div class="row">
    <div class="col-sm-12">
        <?= \app\modules\comments\widgets\CommentWidget::widget([
            'title' => 'Comments',
            'item_id' => $anomaly->id,
            'item_type' => 'anomaly',
        ]) ?>
    </div>
</div>
<?php endif ?>