<?php

/* @var $this yii\web\View */
/* @var $static \app\models\StaticWH */

$this->title = "Static {$static->title} | Wormhole Scanner";
?>
<div class="row">
    <div class="col-md-7 anomaly-wrapper">
        <h1>Static <?= $static->title ?></h1>
        <div class="text-info">

            <h3>Wormhole info</h3>
            <table>
                <tbody>
                    <tr>
                        <td><img src="/images/max_time.png" /></td>
                        <td><b>Max Stable Time</b>: <?= $static->max_stable_time ?> h<br>The maximum amount of time a wormhole will stay open</td>
                    </tr>
                    <tr>
                        <td><img src="/images/max_mass.png" /></td>
                        <td><b>Max Stable Mass</b>: <?= $static->max_stable_mass ?> kg<br>The maximum amount of mass a wormhole can transit before collapsing</td>
                    </tr>
                    <tr>
                        <td><img src="/images/max_regen.png" /></td>
                        <td><b>Max Mass Regeneration</b>: <?= $static->max_mass_regeneration ?> kg<br>The amount of mass a wormhole regenerates per cycle</td>
                    </tr>
                    <tr>
                        <td><img src="/images/jump_mass.png" /></td>
                        <td><b>Max Jump Mass</b>: <?= $static->max_jump_mass ?> kg<br>The maximum amount of mass that can transit a wormhole in one go</td>
                    </tr>
                </tbody>
            </table>

            <h3>General Messages</h3>
            <h4>Uptime</h4>
            <ul class="regular-list">
                <li>"This wormhole has not yet begun its natural cycle of decay and should last at least another day." means 24+ hours</li>
                <li>"This wormhole is beginning to decay, and probably won't last another day." means under 24 hours</li>
                <li>"This wormhole is reaching the end of its natural lifetime." less than 4 hours, not verified.</li>
                <li>"This wormhole is on the verge of dissipating into the ether." -- not sure the timer</li>
            </ul>

            <h4>Mass</h4>
            <ul class="regular-list">
                <li><i>"This wormhole has not yet had its stability significantly disrupted by ships passing through it."</i><br />
                More then 1/2 of the total mass. <br />
                Approx. <b><?= number_format((\app\helpers\FormatHelper::toInt($static->max_stable_mass) / 2)) ?></b> kg</li>

                <li><i>"This wormhole has had its stability reduced by ships passing through it, but not to a critical degree yet."</i><br />
                Less then 1/2 but more then 1/10th of the total mass. <br />
                Approx. <b><?= number_format((\app\helpers\FormatHelper::toInt($static->max_stable_mass) / 2)) ?></b> kg to <b><?= number_format((\app\helpers\FormatHelper::toInt($static->max_stable_mass) / 10)) ?></b> kg</li>

                <li><i>"This wormhole has had its stability critically disrupted by the mass of numerous ships passing through and is on the verge of collapse."</i><br />
                Less than 0.1 of the total mass.<br />
                Approx. <b><?= number_format((\app\helpers\FormatHelper::toInt($static->max_stable_mass) / 10)) ?></b> kg</li>
            </ul>

        </div>
    </div>
    <div class="col-md-5">
        <div class="system-info">
            <h3><?= $static->title ?></h3>
            <p><?= $static->description ?></p>
        </div>
    </div>
</div>
