<?php


namespace app\components;


use app\components\zKillboard\models\Attacker;
use app\components\zKillboard\models\Kill;
use app\components\zKillboard\models\Victim;
use app\models\ZAttacker;
use app\models\ZKill;
use app\models\ZVictim;
use Yii;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;

class ZKillConsister
{
    public function consistZAttacker(Attacker $attacker, ZKill $zkill)
    {
        $zattacker = new ZAttacker();
        $zattacker->setAttributes([
            'zkill_id' => $zkill->killmail_id,
            'npc' => $attacker->isNpc() ? 1 : 0,
            'final_blow' => $attacker->final_blow ? 1 : 0,
            'faction_id' => $attacker->faction_id,
            'ship_type_id' => $attacker->ship_type_id,
            'weapon_type_id' => $attacker->weapon_type_id,
            'character_id' => $attacker->character_id,
            'corporation_id' => $attacker->corporation_id,
            'alliance_id' => $attacker->alliance_id,
        ]);
        if ($attacker->character_id) {
            $zattacker->character_name = ArrayHelper::getValue(Yii::$app->evesso->characterInfo($attacker->character_id), 'name');
            $zattacker->character_picture = Yii::$app->evesso->characterPortrait($attacker->character_id);
        }
        if ($attacker->corporation_id) {
            $zattacker->corporation_name = ArrayHelper::getValue(Yii::$app->evesso->corporationInfo($attacker->corporation_id), 'name');
            $zattacker->corporation_picture = Yii::$app->evesso->corporationIcon($attacker->corporation_id);
        }
        if ($attacker->alliance_id) {
            $zattacker->alliance_name = ArrayHelper::getValue(Yii::$app->evesso->allianceInfo($attacker->alliance_id), 'name');
            $zattacker->alliance_picture = Yii::$app->evesso->allianceIcon($attacker->alliance_id);
        }
        if (!$zattacker->save()) {
            Yii::error([
                'msg' => 'Error saving ZAttacker',
                'errors' => $zattacker->errors,
            ]);
            if ($attacker->final_blow) {
                throw new ErrorException('Error saving final blow ZAttacker');
            }
            return null;
        }

        return $zattacker;
    }

    public function consistZVictim(Victim $victim, ZKill $zkill)
    {
        $zvictim = new ZVictim();
        $zvictim->setAttributes([
            'zkill_id' => $zkill->killmail_id,
            'ship_type_id' => $victim->ship_type_id,
            'character_id' => $victim->character_id,
            'corporation_id' => $victim->corporation_id,
            'alliance_id' => $victim->alliance_id,
        ]);
        if ($victim->character_id) {
            $zvictim->character_name = ArrayHelper::getValue(Yii::$app->evesso->characterInfo($victim->character_id), 'name');
            $zvictim->character_picture = Yii::$app->evesso->characterPortrait($victim->character_id);
        }
        if ($victim->corporation_id) {
            $zvictim->corporation_name = ArrayHelper::getValue(Yii::$app->evesso->corporationInfo($victim->corporation_id), 'name');
            $zvictim->corporation_picture = Yii::$app->evesso->corporationIcon($victim->corporation_id);
        }
        if ($victim->alliance_id) {
            $zvictim->alliance_name = ArrayHelper::getValue(Yii::$app->evesso->allianceInfo($victim->alliance_id), 'name');
            $zvictim->alliance_picture = Yii::$app->evesso->allianceIcon($victim->alliance_id);
        }

        if (!$zvictim->save()) {
            Yii::error([
                'msg' => 'Error saving ZVictim',
                'errors' => $zvictim->errors,
            ]);
            throw new ErrorException('Error saving ZVictim');
        }

        return $zvictim;
    }

    public function consistZKill(Kill $kill)
    {
        $zkill = new ZKill();
        $zkill->setAttributes([
            'killmail_id' => $kill->killID,
            'system_id' => $kill->killmail->solar_system_id,
            'npc' => $kill->zkb->npc ? 1 : 0,
            'solo' => $kill->zkb->solo ? 1 : 0,
            'awox' => $kill->zkb->awox ? 1 : 0,
            'time' => $kill->killmail->killmail_time,
        ]);
        if (!$zkill->save()) {
            Yii::error([
                'msg' => 'Error saving ZKill',
                'errors' => $zkill->errors,
            ]);
            throw new ErrorException('Error saving ZKill');
        }

        // сохраняем жертву
        $this->consistZVictim($kill->killmail->victim, $zkill);
        // сохраняем атакующих
        foreach ($kill->killmail->attackers as $attacker) {
            /** @var Attacker */
            $this->consistZAttacker($attacker, $zkill);
        }

        return $zkill;
    }
}