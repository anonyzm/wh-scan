<?php

namespace app\components\zKillboard\traits;

use Yii;

/**
 * Trait FromArrayFactoryTrait
 * @package app\components\zKillboard\traits
 */
trait FromArrayFactoryTrait {
    /**
     * @param array $params
     * @return object
     */
    public static function create(array $params) {
        $model = new static();
        $model->setAttributes($params);
        if (!$model->validate()) {
            Yii::error([
                'msg' => 'Error validating model ' . self::class,
                'errors' => $model->errors,
            ]);
            return null;
        }
        return $model;
    }
}