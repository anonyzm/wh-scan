<?php

namespace app\components\zKillboard\models;

use app\components\zKillboard\traits\FromArrayFactoryTrait;

/**
 * Class Zkb
 * @package app\components\zKillboard\models
 */
class Zkb extends \yii\base\Model
{
    use FromArrayFactoryTrait;

    public $locationID;
    public $hash;
    public $fittedValue;
    public $totalValue;
    public $points;
    public $npc;
    public $solo;
    public $awox;
    public $labels;
    public $href;

    public function rules()
    {
        return [
            [['hash', 'href'], 'string'],
            [['npc', 'solo', 'awox'], 'boolean'],
            [['locationID', 'points'], 'integer'],
            [['fittedValue', 'totalValue'], 'double'],
            [['labels'], 'safe'],
        ];
    }

    /**
     * @return bool
     */
    public function isSolo() {
        return $this->solo === true;
    }

    /**
     * @return bool
     */
    public function isNpc() {
        return $this->npc === true;
    }
}