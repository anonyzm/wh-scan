<?php

namespace app\components\zKillboard\models;

use Yii;
use yii\helpers\ArrayHelper;

class Killmail extends \yii\base\Model
{
    /**
     * @var Attacker[]
     */
    public $attackers = [];
    /**
     * @var integer
     */
    public $killmail_id;
    /**
     * @var integer
     */
    public $killmail_time;
    /**
     * @var integer
     */
    public $solar_system_id;
    /**
     * @var Victim
     */
    public $victim;

    public function rules()
    {
        return [
            [['killmail_id', 'killmail_time', 'solar_system_id'], 'required'],
            [['killmail_id', 'killmail_time', 'solar_system_id'], 'integer'],
            [['victim', 'attackers'], 'safe'],
        ];
    }

    public static function create(array $params)
    {
        $attackersArray = ArrayHelper::getValue($params, 'attackers', []);
        $attackers = [];
        foreach ($attackersArray as $attacker) {
            $attackers[] = Attacker::create($attacker);
        }

        $model = new self();
        $model->setAttributes([
            'killmail_id' => ArrayHelper::getValue($params, 'killmail_id'),
            'killmail_time' => date("U", strtotime(ArrayHelper::getValue($params, 'killmail_time'))),
            'solar_system_id' => ArrayHelper::getValue($params, 'solar_system_id'),
            'attackers' => $attackers,
            'victim' => Victim::create(ArrayHelper::getValue($params, 'victim')),
        ]);
        if (!$model->validate()) {
            Yii::error([
                'msg' => 'Error validating model ' . self::class,
                'errors' => $model->errors,
            ]);
            return null;
        }
        return $model;
    }
}