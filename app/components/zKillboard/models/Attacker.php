<?php

namespace app\components\zKillboard\models;

/**
 * Class Attacker
 * @package app\components\zKillboard\models
 */
class Attacker extends BaseCapsuleer
{
    public $faction_id;
    public $damage_done;
    public $final_blow;
    public $security_status;
    public $weapon_type_id;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['damage_done', 'final_blow'], 'required'],
            [['damage_done', 'weapon_type_id', 'faction_id'], 'integer'],
            ['security_status', 'double'],
            ['final_blow', 'boolean'],
        ]);
    }

    /**
     * @return bool
     */
    public function isFinalBlow() {
        return $this->final_blow === true;
    }

    /**
     * @return bool
     */
    public function isNpc() {
        return empty($this->character_id) && !empty($faction_id);
    }
}