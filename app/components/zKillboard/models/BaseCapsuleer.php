<?php

namespace app\components\zKillboard\models;

use app\components\zKillboard\traits\FromArrayFactoryTrait;
use Yii;

/**
 * Class BaseCapsuleer
 * @package app\components\zKillboard\models
 */
class BaseCapsuleer extends \yii\base\Model
{
    use FromArrayFactoryTrait;

    public $alliance_id;
    public $corporation_id;
    public $character_id;
    public $ship_type_id;

    public function rules()
    {
        return [
            [['character_id', 'alliance_id', 'corporation_id', 'character_id', 'ship_type_id'], 'integer'],
        ];
    }
}