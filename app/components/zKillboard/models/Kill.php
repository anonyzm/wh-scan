<?php

namespace app\components\zKillboard\models;

use Yii;
use yii\helpers\ArrayHelper;

class Kill extends \yii\base\Model
{
    /**
     * @var integer
     */
    public $killID;
    /**
     * @var Killmail
     */
    public $killmail;
    /**
     * @var Zkb
     */
    public $zkb;

    public function rules()
    {
        return [
            ['killID', 'required'],
            ['killID', 'integer'],
            [['killmail', 'zkb'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return Kill|null
     * @throws \Exception
     */
    public static function create(array $params)
    {
        $model = new self();
        $model->setAttributes([
            'killID' => ArrayHelper::getValue($params, 'killID'),
            'killmail' => Killmail::create($params['killmail']),
            'zkb' => Zkb::create($params['zkb']),
        ]);
        if (!$model->validate()) {
            Yii::error([
                'msg' => 'Error validating model ' . self::class,
                'errors' => $model->errors,
            ]);
            return null;
        }
        return $model;
    }
}