<?php

namespace app\components\zKillboard\models;

/**
 * Class Victim
 * @package app\components\zKillboard\models
 */
class Victim extends BaseCapsuleer
{
    public $damage_taken;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['damage_taken'], 'required'],
            ['damage_taken', 'integer'],
        ]);
    }
}