<?php
namespace app\components\zKillboard;

use app\components\zKillboard\models\Kill;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class ZKillboard
 * @package app\components
 */
class ZKillboard extends Component
{
    public $url = 'https://redisq.zkillboard.com/listen.php';
    public $queueId = 'whscan';

    /**
     * @return Kill|null
     * @throws \Exception
     */
    public function listen() {
        $context = stream_context_create([
            'http' => [
                'timeout' => 20,
            ],
        ]);
        $result = file_get_contents("{$this->url}?queueID={$this->queueId}", false, $context);
        // file_put_contents('/app/zkill_'.time(), $result);
        $json = json_decode($result, true);
        $package = ArrayHelper::getValue($json, 'package');
        if (!empty($package)) {
            return Kill::create($package);
        }

        return null;
    }
}
