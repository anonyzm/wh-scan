<?php
namespace app\components;

use app\models\EveSsoUser;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Class EveSso
 * @package app\components
 * @property string|null $characterName
 * @property string|null $characterAvatar
 * @property EveSsoUser|null $user
 */
class EveSso extends Component {

    public $appID;
    public $secretKey;
    public $callbackUrl;

    protected $_ssoServer = 'https://login.eveonline.com/oauth/';
    protected $_esiServer = 'https://esi.evetech.net/latest/';
    protected $_imageServer = 'https://imageserver.eveonline.com/';

    const COOKIE_PARAM_NAME = 'EVESSOUser';
    const ADMIN_CHARACTER_NAME = 'Ano Woodaxe';

    /**
     * @var null| EveSsoUser $_user
     */
    protected $_user = null;

    public function init()
    {
        if ($this->isWeb()) {
            $cookies = Yii::$app->request->cookies;
            $cookieAuthToken = $cookies->getValue(self::COOKIE_PARAM_NAME);

            if ($cookieAuthToken) {
                $this->_user = EveSsoUser::findByToken($cookieAuthToken);
            }
        }
    }

    /**
     * @param $token
     * @return EveSsoUser|bool|null
     */
    public function login($code_response) {
        $token = ArrayHelper::getValue($code_response, 'access_token');
        $client = new Client(['baseUrl' => $this->_ssoServer.'verify']);
        $request = $client->createRequest()
            ->setMethod('get')
            ->setHeaders(['content-type' => 'application/json'])
            ->addHeaders(['Authorization' => 'Bearer '.$token]);

        $response = $request->send();
        if (!$response->isOk) {
            Yii::error('EveSso.login ERROR: '.$response->content);
            return false;
        }

        $data = $response->data;
        $ssoUser = EveSsoUser::findOne(ArrayHelper::getValue($data, 'CharacterID'));
        if (!$ssoUser) {
            $ssoUser = new EveSsoUser();
        }
        if ($ssoUser->load([
            'id' => ArrayHelper::getValue($data, 'CharacterID'),
            'name' => ArrayHelper::getValue($data, 'CharacterName'),
            'token' => $token,
            'refreshToken' => ArrayHelper::getValue($code_response, 'refresh_token'),
            'scopes' => Json::encode(explode(' ', ArrayHelper::getValue($data, 'Scopes'))),
            'token_type' => ArrayHelper::getValue($code_response, 'token_type'),
            //'expires_at' => strtotime(ArrayHelper::getValue($data, 'ExpiresOn')),
            'expires_at' => time() + ArrayHelper::getValue($code_response, 'expires_in'),
        ], '')) {
            if ($ssoUser->validate()) {
                $this->_user = EveSsoUser::findOne(['id' => $ssoUser->id]);
                Yii::error($ssoUser->cookieAuthToken.' - '.$this->_user->cookieAuthToken);
                if (!($this->_user instanceof EveSsoUser)) {
                    if ($ssoUser->save()) {
                        $this->_user = $ssoUser;
                    }
                    else {
                        return false;
                    }
                }
            }
            else {
                Yii::error('EveSso.login ERROR validating EveSsoUser model: '.json_encode($ssoUser->errors));
            }
        }

        return $this->_user;
    }

    public function verifyCode($code) {
        $client = new Client(['baseUrl' => $this->_ssoServer.'token']);
        $request = $client->createRequest()
            ->setMethod('post')
            ->setHeaders(['content-type' => 'application/json'])
            ->addHeaders(['Authorization' => 'Basic '.base64_encode("{$this->appID}:{$this->secretKey}")])
            ->addData([
                'grant_type' => 'authorization_code',
                'code' => $code,
            ]);
        $response = $request->send();
        if(!$response->isOk) {
            return null;
        }

        return $response->data;
    }

    public function refreshToken() {
        $client = new Client(['baseUrl' => $this->_ssoServer.'token']);
        $request = $client->createRequest()
            ->setMethod('post')
            ->setHeaders(['content-type' => 'application/json'])
            ->addHeaders(['Authorization' => 'Basic '.base64_encode("{$this->appID}:{$this->secretKey}")])
            ->addData([
                'grant_type' => 'refresh_token',
                'refresh_token' => $this->user->refreshToken,
            ]);
        $response = $request->send();
        if(!$response->isOk) {
            return false;
        }
        /**
         * {
            "access_token":"MXP...tg2",
            "token_type":"Bearer",
            "expires_in":1200,
            "refresh_token":"gEy...fM0"
        }
         */
        $data = $response->data;

        $this->user->token = ArrayHelper::getValue($data, 'access_token');
        $this->user->token_type = ArrayHelper::getValue($data, 'token_type');
        $this->user->refreshToken = ArrayHelper::getValue($data, 'refresh_token');
        if(!$this->user->save() || !$this->user->save()) {
            return false;
        }

        return true;
    }

    public function getLoginUrl($scope = '') {
        return $this->_ssoServer.'authorize?'.
            http_build_query([
            'response_type' => 'code',
            'redirect_uri' => $this->callbackUrl,
            'client_id' => $this->appID,
            'scope' => $scope,
            'state' => md5($this->appID.microtime()),
        ]);
    }

    public function getCharacterAvatar($character_id = null, $width = 32) {
        if(is_null($character_id)) {
            if(!($this->_user instanceof EveSsoUser)) {
                return null;
            }
            $character_id = $this->_user->id;
        }

        return $this->_imageServer."Character/{$character_id}_{$width}.jpg";
    }

    public function getTypeIcon($type_id) {
        return ArrayHelper::getValue(Yii::$app->params, 'siteDomain', '') . "/images/eve/types/{$type_id}_64.png";
    }

    public function getCharacterName() {
        if(!($this->_user instanceof EveSsoUser)) {
            return null;
        }

        return $this->_user->name;
    }

    public function isLoggedIn() {
        if($this->_user instanceof EveSsoUser) {
            return !empty($this->_user->id);
        }

        return false;
    }

    public function isAdmin() {
        if($this->_user instanceof EveSsoUser) {
            return ($this->_user->name === self::ADMIN_CHARACTER_NAME);
        }

        return false;
    }

    public function getUser() {
        return $this->_user;
    }

    public function notifications() {
        /*{
          "application/json": [
            {
              "notification_id": 1,
              "type": "InsurancePayoutMsg",
              "sender_id": 1000132,
              "sender_type": "corporation",
              "timestamp": "2017-08-16T10:08:00Z",
              "is_read": true,
              "text": "amount: 3731016.4000000004\\nitemID: 1024881021663\\npayout: 1\\n"
            }
          ]
        }*/
        $notifications = $this->_esiCall("characters/{$this->user->id}/notifications/");
        ArrayHelper::multisort($notifications, 'notification_id', SORT_ASC);
        return $notifications;
    }

    /**
     * @param $character_id
     * @return string
     */
    public function characterPortrait($character_id) {
        return ArrayHelper::getValue($this->_esiCall("characters/{$character_id}/portrait/"), 'px64x64');
    }

    /**
     * @param $character_id
     * @return string
     */
    public function characterInfo($character_id) {
        /*{
          "corporation_id": 1000124,
          "birthday": "2004-10-25T10:47:00Z",
          "name": "Eimmulf Agdila",
          "gender": "male",
          "race_id": 2,
          "bloodline_id": 4,
          "description": "",
          "ancestry_id": 23,
          "security_status": 0
        }*/
        return $this->_esiCall("characters/{$character_id}/");
    }

    /**
     * @param $character_id
     * @return string
     */
    public function corporationIcon($corporation_id) {
        return ArrayHelper::getValue($this->_esiCall("corporations/{$corporation_id}/icons/"), 'px64x64');
    }

    public function corporationInfo($corporation_id) {
        /*{
          "name": "CONCORD",
          "ticker": "CONCO",
          "member_count": 164,
          "ceo_id": 3004033,
          "tax_rate": 0,
          "creator_id": 1,
          "description": "CONCORD was jointly formed a century ago by all the empires. The intention of it was to foster the fragile relationship between the empires and act as a meeting place where they could discuss their differences and hammer out a compromise solution. CONCORD has since taken on a life of its own, proud of its independence and determined to uphold its duties.",
          "url": "",
          "home_station_id": 60012256,
          "shares": 100000000
        }*/
        return $this->_esiCall("corporations/{$corporation_id}/");
    }

    public function allianceInfo($alliance_id) {
        /*{
          "application/json": {
            "name": "C C P Alliance",
            "ticker": "<C C P>",
            "creator_id": 12345,
            "creator_corporation_id": 45678,
            "executor_corporation_id": 98356193,
            "date_founded": "2016-06-26T21:00:00Z"
          }
        }*/
        return $this->_esiCall("alliances/{$alliance_id}/");
    }

    public function allianceIcon($alliance_id) {
        /*{
          "application/json": {
            "px64x64": "https://imageserver.eveonline.com/Alliance/503818424_64.png",
            "px128x128": "https://imageserver.eveonline.com/Alliance/503818424_128.png"
          }
        }*/
        return ArrayHelper::getValue($this->_esiCall("alliances/{$alliance_id}/icons/"), 'px64x64');
    }

    public function planet($planet_id) {
        /*{
          "application/json": {
            "planet_id": 40000046,
            "name": "Akpivem III",
            "type_id": 13,
            "position": {
              "x": -189226344497,
              "y": 9901605317,
              "z": -254852632979
            },
            "system_id": 30000003
          }
        }*/
        return $this->_esiCall("universe/planets/{$planet_id}/");
    }

    public function stationInfo($station_id) {
        /*{
          "station_id": 60008494,
          "name": "Amarr VIII (Oris) - Emperor Family Academy",
          "type_id": 1932,
          "position": {
            "x": -518583951360,
            "y": 30256619520,
            "z": 1042895708160
          },
          "system_id": 30002187,
          "reprocessing_efficiency": 0.5,
          "reprocessing_stations_take": 0.05,
          "max_dockable_ship_volume": 50000000,
          "office_rental_cost": 446736802,
          "services": [
            "bounty-missions",
            "courier-missions",
            "interbus",
            "reprocessing-plant",
            "market",
            "stock-exchange",
            "cloning",
            "repair-facilities",
            "factory",
            "fitting",
            "news",
            "insurance",
            "docking",
            "office-rental",
            "loyalty-point-store",
            "navy-offices",
            "security-offices"
          ],
          "owner": 1000086,
          "race_id": 4
        }*/
        return $this->_esiCall("universe/stations/{$station_id}/");
    }

    public function systemInfo($system_id) {
        return $this->_esiCall("universe/systems/{$system_id}/");
    }

    public function killmail($killmail_hash, $killmail_id) {
        $killmail_hash = trim($killmail_hash);
        $killmail_id = trim($killmail_id);
        /*{
          "killmail_id": 67818060,
          "killmail_time": "2018-02-04T21:38:39Z",
          "victim": {
                    "damage_taken": 4750,
            "ship_type_id": 32872,
            "character_id": 92451062,
            "corporation_id": 98450101,
            "items": [
              {
                  "item_type_id": 4405,
                "singleton": 0,
                "flag": 13,
                "quantity_dropped": 1
              },
            ],
            "position": {
                        "x": 1324957501503.4878,
              "y": 250031922111.7487,
              "z": -1032540468200.919
            }
          },
          "attackers": [
            {
                "security_status": -2.1,
              "final_blow": false,
              "damage_done": 2391,
              "character_id": 1741552207,
              "corporation_id": 98457503,
              "alliance_id": 99006997,
              "ship_type_id": 17928,
              "weapon_type_id": 3178
            },

          ],
          "solar_system_id": 30003333
        }*/
        return $this->_esiCall("killmails/{$killmail_id}/{$killmail_hash}/");
    }

    public function typeInfo($type_id) {
        return $this->_esiCall("universe/types/{$type_id}/");
    }

    /**
     * @param $endpoint
     * @param array $data
     * @param string $method
     * @return false|mixed
     */
    protected function _esiCall($endpoint, $data = [], $method = 'get')
    {
        $cacheKey = $method . '_' . $endpoint . '_' . json_encode($data);
        //return Yii::$app->cache->getOrSet($cacheKey, function () use ($endpoint, $data, $method) {
            return $this->_esiInnerCall($endpoint, $data, $method);
        //}, 10800);
    }

    /**
     * @param $endpoint
     * @param array $data
     * @param string $method
     * @return mixed|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    protected function _esiInnerCall($endpoint, $data = [], $method = 'get') {
        if ($this->isWeb()) {
            if (time() >= $this->user->expires_at) {
                $this->refreshToken();
            }
            $data = ArrayHelper::merge(['token' => $this->user->token], $data);
        }
        Yii::error($data);

        $url = $this->_esiServer.$endpoint;
        if(strtolower($method) === 'get') {
            $url .= '?'.http_build_query($data);
        }
        $client = new Client(['baseUrl' => $url]);
        $request = $client->createRequest()
            ->setMethod($method)
            ->setHeaders(['content-type' => 'application/json']);

        if ($this->isWeb()) {
            $request->addHeaders(['Authorization' => 'Basic '.base64_encode("{$this->appID}:{$this->secretKey}")]);
        }

        $response = $request->send();
        if(!$response->isOk) {
            Yii::error("\n_esiCall [{$endpoint}]: {$response->content}\n\n");
            return null;
        }

        return $response->data;
    }

    /**
     * @return bool
     */
    protected function isWeb() {
        return Yii::$app->id === 'basic';
    }
}
