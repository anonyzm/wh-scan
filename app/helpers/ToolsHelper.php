<?php
namespace app\helpers;

use app\models\Effect;
use app\models\EveType;
use app\models\Planet;
use app\models\Ship;
use app\models\StaticWH;
use app\models\System;
use app\models\SystemStatic;
use app\models\ZKill;
use Sunra\PhpSimple\HtmlDomParser;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Class ToolsHelper
 * @package app\helpers
 */
class ToolsHelper {
    /**
     * Импорт в базу всех WH систем с http://www.ellatha.com
     * @throws \yii\db\Exception
     */
    public static function whList() {
        $html = file_get_contents('http://www.ellatha.com/eve/wormholelist.asp');
        $pattern = '|<a href="wormholelistview\.asp\?key=([^"]+)"\>Wormhole (\w{1}\d{3})\<\/a\>|si';
        preg_match_all($pattern, $html, $statics, PREG_SET_ORDER);

        $to_insert = [];
        foreach($statics as $static) {
            $title = $static[2];
            echo $title.PHP_EOL;

            $html = file_get_contents('http://www.ellatha.com/eve/wormholelistview.asp?key='.$static[1]);
            $dom = HtmlDomParser::str_get_html($html);

            $table = $dom->find('table[id=Display]', 0)->find('table', 0)->find('table', 0);

            preg_match('|\<\/b\>\s*:\s*([\d\,]+)\w*\&|si', $table->find('tr', 3)->find('td', 0)->innertext(), $max_mass);
            $max_mass = $max_mass[1];

            preg_match('|\<\/b\>\s*:\s*([\d\,]+)\w*\&|si', $table->find('tr', 2)->find('td', 0)->innertext(), $max_time);
            $max_time = $max_time[1];

            preg_match('|\<\/b\>\s*:\s*([\d\,]+)\w*\&|si', $table->find('tr', 4)->find('td', 0)->innertext(), $regen);
            $regen = $regen[1];

            preg_match('|\<\/b\>\s*:\s*([\d\,]+)\w*\&|si', $table->find('tr', 5)->find('td', 0)->innertext(), $jump_mass);
            $jump_mass = $jump_mass[1];

            $to_insert[] = [
                'title' => $title,
                'description' => str_replace('&nbsp;', '', $table->find('tr', 1)->find('td', 1)->innertext()),
                'max_stable_mass' => $max_mass,
                'max_stable_time' => $max_time,
                'max_jump_mass' => $jump_mass,
                'max_mass_regeneration' => $regen,
            ];
        }

        \Yii::$app->db->createCommand()->batchInsert('static', ['title', 'description', 'max_stable_mass', 'max_stable_time', 'max_jump_mass', 'max_mass_regeneration'], $to_insert)->execute();
    }

    /**
     * Линковка статиков к системам WH с http://www.ellatha.com
     * @throws \yii\db\Exception
     */
    public static function whLink() {
        $statics_tmp = StaticWH::find()->all();
        $statics = [];
        foreach ($statics_tmp as $static) {
            $statics[$static->title] = $static->id;
        }

        $systems = System::find()->all();
        foreach($systems as $key=>$s) {
            echo "{$key}{$s->id}";
            $html = file_get_contents('http://www.ellatha.com/eve/WormholeSystemview.asp?key='.$s->id);

            $pattern = '|\<a href="wormholelistview.asp\?key=Wormhole\+(\w{1}\d{3})"\>(\w{1}\d{3})\<\/a\>|si';
            preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);

            $to_insert = [];
            foreach($matches as $match) {
                echo " {$match[1]}";
                $to_insert[] = [
                    $s->id,
                    $statics[$match[1]],
                ];
            }
            echo PHP_EOL;

            \Yii::$app->db->createCommand()->batchInsert('system_static', ['system_id', 'static_id'], $to_insert)->execute();
        }
    }

    /**
     * Получаем ID систем в рамках EVE API
     * @param int $start
     */
    public static function systemLinkIds($start = 31000000) {
        $system_ids = json_decode(file_get_contents('https://esi.evetech.net/latest/universe/systems/?datasource=tranquility'), true);
        foreach($system_ids as $system_id) {
            if($system_id < $start) {
                continue;
            }

            try {
                $systemJson = file_get_contents("https://esi.evetech.net/latest/universe/systems/{$system_id}/?datasource=tranquility&language=en-us");
                $systemObject = json_decode($systemJson, true);
                $system = System::getByName($systemObject['name']);
                if($systemObject && $system) {
                    /* @var System $system */
                    if(empty($system->external_id)) {
                        $system->external_id = $systemObject['system_id'];
                        $system->CCP_object = $systemJson;
                        if ($system->validate() && $system->save()) {
                            echo "#{$system_id} [DONE] - " . $system->title . PHP_EOL;
                        } else {
                            echo "#{$system_id} [ERROR_SAVE] - " . json_encode($system->errors) . PHP_EOL;
                        }
                    } else {
                        echo "#{$system_id} [SKIPPING] - " . $system->title . PHP_EOL;
                    }
                }
                else {
                    echo "#{$system_id} [ERROR_FIND]".PHP_EOL;
                }
            }
            catch (\Exception $e) {
                echo "#{$system_id} [ERROR] - ".$e->getMessage().PHP_EOL;
            }

            usleep(500);
        }
    }

    /**
     * Запрос к API zKillboard
     * @param $url
     * @return mixed|null
     */
    public static function zkillApi($url) {
        echo "\t$url\n";
        $siteUrl = rtrim(ArrayHelper::getValue(\Yii::$app->params, 'siteDomain', 'https://google.com/'), '/').'/';
        $adminEmail = ArrayHelper::getValue(\Yii::$app->params, 'adminEmail', 'admin@google.com');
        $userAgent = "{$siteUrl} Maintainer: Leonid {$adminEmail}";
        $client = new Client(['baseUrl' => $url]);
        $request = $client->createRequest()
            ->setMethod('get')
            ->setHeaders(['content-type' => 'application/json'])
            ->addHeaders(['User-Agent' => $userAgent]);
        $response = $request->send();
        if(!$response->isOk) {
            return null;
        }

        return $response->data;
    }

    /**
     * Собираем киллы по системам WH с https://zkillboard.com
     * @param null $pastSeconds
     * @param int $debug
     */
    public static function gatherSystemKills($pastSeconds = null , $debug = 0) {
        $systems = System::find()->all();
        if (is_null($pastSeconds)) {
            $pastSeconds = 3600;
        }
        foreach ($systems as $system) {
            try {
                /* @var System $system */
                if (!empty($system->external_id)) {
                    $zkillObjectList = ToolsHelper::zkillApi("https://zkillboard.com/api/solarSystemID/{$system->external_id}/pastSeconds/{$pastSeconds}/");
                    if(is_array($zkillObjectList)) {
                        $exists = 0;
                        $new = 0;
                        $errors = 0;
                        foreach ($zkillObjectList as $zkillObject) {
                            try {
                                $zKill = ZKill::findOne(['killmail_id' => ArrayHelper::getValue($zkillObject, 'killmail_id')]);
                                if (!$zKill) {
                                    $zKill = new ZKill();
                                    $zKill->killmail_id = ArrayHelper::getValue($zkillObject, 'killmail_id');
                                    $zKill->time = time() - $pastSeconds;
                                    $zKill->npc = ArrayHelper::getValue($zkillObject, 'zkb.npc', false) ? 1 : 0;
                                    $zKill->fittedValue = ArrayHelper::getValue($zkillObject, 'zkb.fittedValue');
                                    $zKill->system_id = $system->external_id; // в понятиях CCP

                                    if ($zKill->validate() && $zKill->save()) {
                                        $new++;
                                        if($debug) {
                                            echo "\t{$zKill->killmail_id} [DONE]" . PHP_EOL;
                                        }
                                    }
                                    else {
                                        $errors++;
                                        if($debug) {
                                            echo "\tZKill [ERROR SAVE]\n\t\tERRORS: " . json_encode($zKill->errors). "\n\t\tATTRIBUTES: " . json_encode($zKill->getAttributes()) . PHP_EOL;
                                        }
                                    }
                                }
                                else {
                                    $exists++;
                                    if ($debug) {
                                        echo "\tzKill:{$zKill->killmail_id} [EXISTS] - " . json_encode($zKill->getAttributes()) . PHP_EOL;
                                    }
                                }
                            }
                            catch (\Exception $e) {
                                $errors++;
                                if($debug) {
                                    echo "\tZKill [ERROR] - " . $e->getMessage() . PHP_EOL;
                                }
                            }
                        }
                        echo "\nNew: {$new} / Old: {$exists} / Errors: {$errors}".PHP_EOL;
                    }
                    else {
                        echo "#{$system->id} [ERROR_JSON]" . PHP_EOL;
                    }
                }
                else {
                    echo "#{$system->id} [ERROR_EMPTY]".PHP_EOL;
                }
            }
            catch (\Exception $e) {
                echo "#{$system->id} [ERROR] - " . $e->getMessage() . PHP_EOL;
            }

            sleep(1);
        }
    }

    /**
     * Синхронизируем корабли из киллов с СCP API
     */
    public static function syncKillShips() {
        $ships = ZKill::find()->select(['ship_id'])->distinct()->asArray()->all();
        foreach ($ships as $ship) {
            $shipJson = file_get_contents("https://esi.evetech.net/latest/universe/types/{$ship['ship_id']}/?datasource=tranquility&language=en-us");
            $shipObject = json_decode($shipJson, true);
            if(is_array($shipObject)) {
                echo ArrayHelper::getValue($shipObject, 'name', 'Unknown ship');

                $shipModel = Ship::findOne($ship['ship_id']);
                if(!$shipModel) {
                    $shipModel = new Ship();
                    $shipModel->id = ArrayHelper::getValue($shipObject, 'type_id', null);
                    $shipModel->title = ArrayHelper::getValue($shipObject, 'name', false);
                    $shipModel->description = ArrayHelper::getValue($shipObject, 'description', false);
                    $shipModel->graphic_id = ArrayHelper::getValue($shipObject, 'graphic_id', false);

                    if($shipModel->validate() && $shipModel->save()) {
                        echo " - {$shipModel->id} [DONE]" . PHP_EOL;
                    }
                    else {
                        echo " - {$shipModel->id} [ERROR] - " . json_encode($shipModel->errors) . PHP_EOL;
                    }
                }
                else {
                    echo " - {$shipModel->id} [EXISTS]" . PHP_EOL;
                }
            }

            usleep(500);
        }
    }

    /**
     * Импортируем системы shattered wormholes
     */
    public static function importShatteredSystems() {
        $systems = file(\Yii::getAlias('@app/migrations/files/rhea_wh.csv'));
        foreach ($systems as $key=>$system) {
            /**
             * 0 - title,
             * 1 - class?
             * 2 - effect,
             * 3 - statics,
             * 4 - id,
             * 5 - frigates
             */
            $system = explode(';', $system);
            if(is_array($system) && count($system) === 6) {
                $s = new \app\models\System();
                $s->id = intval($system[4]);
                $s->class_id = (false !== stripos($system[1], '?')) ? intval($system[1]) : null;

                $effect = \app\models\Effect::find()->where(['LIKE', 'title', trim($system[2])])->one();
                $s->effect_id = $effect ? $effect->id : null;
                $s->security = -1;
                $s->title = $system[0];
                $s->description = (trim($system[5]) === 'Yes') ? 'Frigates only' : '';

                $system_id = $s->id;
                try {
                    $system_id = $system[4];
                    $systemJson = file_get_contents("https://esi.evetech.net/latest/universe/systems/{$system_id}/?datasource=tranquility&language=en-us");
                    $systemObject = json_decode($systemJson, true);
                    if($systemObject && $s) {
                        /* @var \app\models\System $s */
                        if(empty($s->external_id)) {
                            $s->external_id = $systemObject['system_id'];
                            $s->CCP_object = $systemJson;
                            if ($s->validate() && $s->save()) {
                                echo "#{$system_id} [DONE] - " . $s->title . PHP_EOL;

                                // parsing statics
                                preg_match_all('|(\w{1}\d{3})|si', $system[3], $matches, PREG_SET_ORDER);
                                if(!empty($matches)) {
                                    foreach ($matches as $match) {
                                        $static = StaticWH::findOne(['title' => $match[1]]);
                                        if($static) {
                                            $systemStatic = new SystemStatic();
                                            $systemStatic->static_id = $static->id;
                                            $systemStatic->system_id = $system_id;
                                            if ($systemStatic->save()) {
                                                echo "\tstatic saved: {$match[1]}".PHP_EOL;
                                            }
                                            else {
                                                echo "\tstatic error: {$match[1]}".PHP_EOL;
                                                die();
                                            }
                                        }
                                        else {
                                            echo "\tstatic unknown: {$match[1]}".PHP_EOL;
                                        }
                                    }
                                }

                            } else {
                                echo "#{$system_id} [ERROR_SAVE] - " . json_encode($s->errors) . PHP_EOL;
                            }
                        } else {
                            echo "#{$system_id} [SKIPPING] - " . $s->title . PHP_EOL;
                        }
                    }
                    else {
                        echo "#{$system_id} [ERROR_FIND]".PHP_EOL;
                    }
                }
                catch (\Exception $e) {
                    echo "#{$system_id} [ERROR] - ".$e->getMessage().PHP_EOL;
                }
            }
            else {
                echo "#{$key} [WRONG_FORMAT] - " . json_encode($system) . PHP_EOL;
            }
        }
    }

    /**
     * Фикс shattered систем после импорта
     */
    public static function fixShatteredSystems() {
        $systems = file(\Yii::getAlias('@app/migrations/files/rhea_wh.csv'));
        foreach ($systems as $key=>$system) {
            /**
             * 0 - title,
             * 1 - class?
             * 2 - effect,
             * 3 - statics,
             * 4 - id,
             * 5 - frigates
             */
            $system = explode(';', $system);
            if(is_array($system) && count($system) === 6) {
                /* @var $s System */
                $s = System::getByName($system[0]);
                echo "System #{$system[0]} - class `{$system[1]}`" . PHP_EOL;
                $s->class_id = (false === stripos($system[1], '?')) ? intval($system[1]) : null;
                if(false !== stripos($system[2], 'Black')) {
                    $s->effect_id = 2;
                    echo "#{$system[0]} - black hole" . PHP_EOL;
                }
                $s->save();
            }
            else {
                echo "#{$key} [WRONG_FORMAT] - " . json_encode($system) . PHP_EOL;
            }
        }
    }

    /**
     * Наполнение типами таблицы 
     */
    public static function importTypes() {
        $pages = 1;
        $page = 1;
        do {
            $typesJson = file_get_contents("https://esi.evetech.net/latest/universe/types/?datasource=tranquility&language=en-us&page={$page}");
            if (!empty($http_response_header)) {
                foreach ($http_response_header as $header) {
                    if (false !== stripos($header, 'x-pages')) {
                        $pages = intval(str_replace('X-Pages: ', '', $header));
                    }
                }
            }

            $types = json_decode($typesJson, true);
            if ($types) {
                foreach ($types as $type_id) {
                    try {
                        $typeJson = file_get_contents("https://esi.evetech.net/latest/universe/types/{$type_id}/?datasource=tranquility&language=en-us");
                        $type = json_decode($typeJson, true);
                        if ($type) {
                            echo "#{$type_id} - {$type['name']} - ";
                            // создание модели
                            $typeModel = new EveType();
                            $typeModel->id = $type_id;
                            $typeModel->group_id = ArrayHelper::getValue($type, 'group_id');
                            $typeModel->title = ArrayHelper::getValue($type, 'name');
                            $typeModel->description = ArrayHelper::getValue($type, 'description');
                            if ($typeModel->validate() && $typeModel->save()) {
                                echo 'OK [' . json_encode($typeModel->getAttributes()) . ']' . PHP_EOL;
                            } else {
                                echo 'ERROR [' . json_encode($typeModel->errors) . ']' . PHP_EOL;
                            }
                        }

                        usleep(100);
                    } catch (\Exception $e) {
                        echo $e->getMessage() . PHP_EOL;
                    }
                }
            }

            $page++;
        } while ($page <= $pages);
    }

    public static function generateTypesCss() {
        $fp = fopen(\Yii::getAlias('@app/web/css/types.css'), 'w+');

        $types = EveType::find()->all();
        foreach ($types as $type) {
            /** @var $type EveType */
            try {
                if(file_exists(\Yii::getAlias("@app/web/images/eve/types/{$type->id}_64.png"))) {
                    $class = ".eve-type-{$type->id} { background-image: url(/images/eve/types/{$type->id}_64.png); }" . PHP_EOL;
                    fputs($fp, $class);
                    echo $type->id . PHP_EOL;
                }
            } catch (\Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }
        }

        fclose($fp);
    }

    public static function linkPlanets() {
        $systems = System::find()->all();
        foreach ($systems as $system) {
            try {
                /** @var System $system */
                $ccpSystem = $system->getCcpObject();
                $system_id = $system->id;
                echo "System: ".$system_id.PHP_EOL;
                $planets = ArrayHelper::getValue($ccpSystem, 'planets', []);
                Planet::deleteAll(['system_id' => $system_id]);
                foreach ($planets as $planet) {
                    try {
                        $planet_id = $planet['planet_id'];
                        $ccpPlanet = file_get_contents("https://esi.evetech.net/latest/universe/planets/{$planet_id}/?datasource=tranquility&language=en-us");
                        $ccpPlanet = Json::decode($ccpPlanet);
                        $p = new Planet([
                            'id' => $planet_id,
                            'name' => $ccpPlanet['name'],
                            'system_id' => $ccpPlanet['system_id'],
                            'type_id' => $ccpPlanet['type_id'],
                            'moons' => '[]',
                        ]);
                        $p->moons = Json::encode(ArrayHelper::getValue($p, 'moons', []));

                        if (!$p->validate() || !$p->save()) {
                            echo "\tError saving planet: {$planet_id}" . PHP_EOL;
                        } else {
                            echo "\t" . $p->name . PHP_EOL;
                        }
                    }
                    catch (\Throwable $e) {
                        echo "Error importing planet: ".$e->getMessage().PHP_EOL;
                    }

                    sleep(1);
                }
            }
            catch(\Exception $e) {
                //echo 'ERROR: '.$e->getMessage().' | '.$e->getFile().':'.$e->getLine().PHP_EOL;
                throw $e;
            }
        }
    }

}