<?php
namespace app\helpers;

class FormatHelper {
    public static function toInt($string) {
        $num = str_replace([',', '.'], '', $string);
        return intval($num);
    }

}