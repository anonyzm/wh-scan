<?php

namespace app\modules\notifications;
use yii\base\BootstrapInterface;
use yii\web\Application;

/**
 * notifications module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\notifications\controllers';

    protected $urlRules = [
        'notifications/get',
    ];

    public function bootstrap($app)
    {
        if($app instanceof Application) {
            \Yii::$app->urlManager->addRules($this->urlRules);
        }
    }
}
