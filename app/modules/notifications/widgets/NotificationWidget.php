<?php
namespace app\modules\notifications\widgets;

use app\modules\notifications\models\Comment;
use yii\bootstrap\Widget;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

class NotificationWidget extends Widget
{
    public function run()
    {
        return $this->render('@app/modules/notifications/views/notifications');
    }
}