<?php

namespace app\modules\notifications\controllers;

use app\components\EveSso;
use app\modules\notifications\models\Notification;
use yii\base\Action;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;

class NotificationController extends Controller
{
    protected $_publicActions = ['index'];

    /**
     * @param Action $action
     * @return bool
     * @throws UnauthorizedHttpException
     */
    public function beforeAction($action)
    {
        if(!\Yii::$app->evesso->isLoggedIn() && !in_array($action->id, $this->_publicActions)) {
            throw new UnauthorizedHttpException('Please log in using EVE SSO!');
        }

        return parent::beforeAction($action);
    }

    public function actionGet()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $character_id = \Yii::$app->evesso->user->id;
        $count = Notification::find()->where(['character_id' => $character_id])->count();

        \Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => EveSso::COOKIE_PARAM_NAME,
            'value' => \Yii::$app->evesso->user->cookieAuthToken,
            'expire' => time() + 86400 * 30,
        ]));

        $notifications = \Yii::$app->evesso->notifications();
        $readCount = count($notifications);
        $notify = [];
        foreach ($notifications as $key=>$notification) {
            $notification['timestamp'] = strtotime($notification['timestamp']);
            if(!Notification::exists($notification['notification_id'])) {
                $model = new Notification();
                $model->load(ArrayHelper::merge($notification, ['character_id' => $character_id]), '');
                if (!$model->validate() || !$model->save()) {
                    var_dump($model->getAttributes());
                    var_dump($model->firstErrors);
                    die();
                }

                // если первый раз, показываем только последние 20 штук
                if($count == 0 && ($readCount - $key) < 20 ) {
                    /** @var Notification $model */
                    $notify[] = [
                        'title' => "{$model->title} ({$model->time}ago)",
                        'text' => $model->description,
                        'icon' => $model->icon,
                    ];

                    break;
                }
                else if($count > 0) {
                    /** @var Notification $model */
                    $notify[] = [
                        'title' => "{$model->title} ({$model->time}ago)",
                        'text' => $model->description,
                        'icon' => $model->icon,
                    ];

                    break;
                }
            }
        }

        return $notify;
    }

    public function actionIndex() {
        return $this->render('index');
    }

}
