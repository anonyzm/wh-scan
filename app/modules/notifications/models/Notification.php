<?php

namespace app\modules\notifications\models;

use app\modules\notifications\helpers\NotificationHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $id
 * @property integer $notification_id
 * @property integer $character_id
 * @property string $title
 * @property string $description
 * @property string $type
 * @property string $time
 * @property string $icon
 * @property integer $sender_id
 * @property string $sender_type
 * @property integer $timestamp
 * @property string $text
 */
class Notification extends \yii\db\ActiveRecord
{
    protected $_icon = null;
    protected $_title = null;
    protected $_description = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_id', 'sender_id', 'timestamp', 'character_id'], 'integer'],
            [['text'], 'string'],
            [['type', 'sender_type'], 'string', 'max' => 255],
        ];
    }

    public function beforeValidate()
    {
        if(!is_numeric($this->timestamp)) {
            $this->timestamp = strtotime($this->timestamp);
        }
        return parent::beforeValidate();
    }

    public static function exists($notification_id) {
        return (self::find()->where(['notification_id' => $notification_id])->count() == 1);
    }

    /**
     * @return null|string
     */
    public function getTitle() {
        if(is_null($this->_title)) {
            $this->_title = NotificationHelper::transformTitle($this);
        }

        return $this->_title;
    }

    /**
     * @return null|string
     */
    public function getDescription() {
        if(is_null($this->_description)) {
            $this->_description = NotificationHelper::transformDescription($this);
        }

        return $this->_description;
    }

    /**
     * @return null|string
     */
    public function getIcon() {
        if(is_null($this->_icon)) {
            $this->_icon = NotificationHelper::transformIcon($this);
        }

        return $this->_icon;
    }

    /**
     * @return string
     */
    public function getTime() {
        $dt = time() - $this->timestamp;
        $interval = [
            'days' => floor($dt / 86400),
            'hours' => floor(($dt % 86400 ) / 3600),
            'min' => floor(($dt % 3600) / 60),
            'sec' => $dt % 60,
        ];
        $intervalStr = '';
        foreach ($interval as $i=>$value) {
            if(!empty($value)) {
                $intervalStr .= "{$value} {$i} ";
            }
        }

        return $intervalStr;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'sender_id' => 'Sender ID',
            'sender_type' => 'Sender Type',
            'timestamp' => 'Timestamp',
            'text' => 'Text',
        ];
    }
}
