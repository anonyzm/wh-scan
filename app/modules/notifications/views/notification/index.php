<?php
/* @var $this yii\web\View */

$this->title = "Notifications | Wormhole Scanner";
?>
<div class="row margin-bottom-large">
    <div class="col-md-7 anomaly-wrapper">
        <h1 style="font-size: 16px">New Feature: browser notifications</h1>
        <p>Introducing new feature - in-browser notifications!</p>
        <p>
            Someone is attacking you refinery while you are offline? Your POS is running out of fuel? Don't worry!<br />
            Now you will always be up do date with any EVE Online notifications. Just open <?= \yii\bootstrap\Html::a('whscan.cf', '/') ?> in your favorite browser and click <b>Login</b> button on this page. Since then you will see all EVE notifications in browser.<br>
            Using this feature is absolutely safe! We DO NOT store any information about your notifications (just the ID of last received notification). All you private information is absolutely safe.
        </p>
    </div>
    <div class="col-md-5">
        <div class="system-info">
            <h3 class="system-name">Login using EVE SSO</h3>
            <p><?= \yii\bootstrap\Html::a('', ['/sso/login', 'scope' => 'esi-characters.read_notifications.v1'], ['class' => 'eve-login-button', 'style' => 'float: none; text-align: left; display: inline-block;'])?></p>
        </div>

    </div>
</div>