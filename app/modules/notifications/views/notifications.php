<?php
/* @var $this \yii\web\View */

$js = <<<JS
var permission,
     timer = setTimeout( function() { permission = "default" }, 500 );
Notification.requestPermission( function(state){ clearTimeout(timer); permission = state } );

setTimeout(function () {
    if(permission === 'granted') {
        notify();
    }
}, 2000);
var i = 1;
function notify() {
    console.log(i++);
    $.get( '/notifications/notification/get', function( data ) {
        for(key in data) {
            var mailNotification = new Notification(data[key].title, {
                //tag : "ache-mail",
                body : data[key].text,
                icon : data[key].icon
            });
        }
        
        setTimeout(notify, 10000);
    });
}
JS;
if(\Yii::$app->evesso->isLoggedIn() && \Yii::$app->evesso->user->canReadNotifications()) {
    $this->registerJs($js);
}
?>