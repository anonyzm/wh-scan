<?php
namespace app\modules\notifications\helpers;

use app\models\EveType;
use app\modules\notifications\models\Notification;
use yii\helpers\ArrayHelper;

class NotificationHelper
{
    /**
    | AllianceCapitalChanged                      |
    | AllWarDeclaredMsg                           |
    | AllWarInvalidatedMsg                        |
    | AllWarRetractedMsg                          |
    | AllyJoinedWarAllyMsg                        |
    | BountyClaimMsg                              |
    | BountyPlacedChar                            |
    | CharAppAcceptMsg                            |
    | CharAppWithdrawMsg                          |
    | CharMedalMsg                                |
    | CharTerminationMsg                          |
    | CloneActivationMsg                          |
    | CloneActivationMsg2                         |
    | ContactAdd                                  |
    | CorpAllBillMsg                              |
    | CorpAppAcceptMsg                            |
    | CorpAppInvitedMsg                           |
    | CorpAppNewMsg                               |
    | CorpFriendlyFireDisableTimerCompleted       |
    | CorpFriendlyFireEnableTimerCompleted        |
    | CorpFriendlyFireEnableTimerStarted          |
    | CorpKicked                                  |
    | CorpNewCEOMsg                               |
    | CorpTaxChangeMsg                            |
    | CorpWarDeclaredMsg                          |
    | CorpWarInvalidatedMsg                       |
    | EntosisCaptureStarted                       |
    | FacWarCorpJoinRequestMsg                    |
    | FacWarCorpLeaveRequestMsg                   |
    | FacWarLPPayoutEvent                         |
    | FWCorpJoinMsg                               |
    | FWCorpLeaveMsg                              |
    | GameTimeAdded                               |
    | GameTimeReceived                            |
    | IncursionCompletedMsg                       |
    | InsuranceExpirationMsg                      |
    | InsuranceFirstShipMsg                       |
    | InsuranceIssuedMsg                          |
    | InsurancePayoutMsg                          |
    | JumpCloneDeletedMsg1                        |
    | JumpCloneDeletedMsg2                        |
    | KillReportFinalBlow                         |
    | KillReportVictim                            |
    | KillRightEarned                             |
    | MissionOfferExpirationMsg                   |
    | MissionTimeoutMsg                           |
    | MoonminingAutomaticFracture                 |
    | MoonminingExtractionFinished                |
    | MoonminingLaserFired                        |
    | notificationTypeMoonminingExtractionStarted |
    | NPCStandingsLost                            |
    | OrbitalAttacked                             |
    | OrbitalReinforced                           |
    | OwnershipTransferred                        |
    | SeasonalChallengeCompleted                  |
    | SovCommandNodeEventStarted                  |
    | SovStationEnteredFreeport                   |
    | SovStructureDestroyed                       |
    | SovStructureReinforced                      |
    | StationServiceDisabled                      |
    | StationServiceEnabled                       |
    | StoryLineMissionAvailableMsg                |
    | StructureAnchoring                          |
    | StructureDestroyed                          |
    | StructureFuelAlert                          |
    | StructureItemsDelivered                     |
    | StructureOnline                             |
    | StructureServicesOffline                    |
    | StructureUnderAttack                        |
    | TowerAlertMsg                               |
    | TowerResourceAlertMsg                       |
     */
    /**
     * @param Notification $notification
     */
    public static function transformTitle(Notification $notification) {
        switch ($notification->type) {
            case 'AllianceCapitalChanged': return 'Alliance capital changed';
            case 'AllWarDeclaredMsg': return 'Alliance declared war';
            case 'AllWarInvalidatedMsg': return 'Alliance war invalidated';
            case 'AllWarRetractedMsg': return 'Alliance retracted war';
            case 'AllyJoinedWarAllyMsg': return 'Alliance joined war';
            case 'BountyClaimMsg': return 'Bounty received';
            case 'BountyPlacedChar': return 'Bounty placed';
            case 'CharAppAcceptMsg': preg_match('|applicationText:\s+(.*)\s*charID:\s+(\d+)\s*corpID:\s+(\d+)|m', $notification->text, $match);
                $character = \Yii::$app->evesso->characterInfo($match[2]);
                return "Application: {$character['name']}";
                //TODO:  закончил тут
            /*case 'CharAppWithdrawMsg': return '';
            case 'CharMedalMsg': return '';
            case 'CharTerminationMsg': return '';
            case 'CloneActivationMsg': return '';*/
            case 'CloneActivationMsg2': return 'Clone activated';
            /*case 'ContactAdd': return '';
            case 'CorpAppAcceptMsg': return '';
            case 'CorpAllBillMsg': return '';
            case 'CorpAppInvitedMsg': return '';
            case 'CorpAppNewMsg': return '';*/
            case 'CorpFriendlyFireDisableTimerCompleted': return '';
            case 'CorpFriendlyFireEnableTimerCompleted': return '';
            case 'CorpFriendlyFireEnableTimerStarted': return '';
            /*case 'CorpKicked': return '';
            case 'CorpNewCEOMsg': return '';
            case 'CorpTaxChangeMsg': return '';
            case 'CorpWarDeclaredMsg': return '';
            case 'CorpWarInvalidatedMsg': return '';
            case 'EntosisCaptureStarted': return '';
            case 'FacWarLPPayoutEvent': return '';*/
            case 'FacWarCorpJoinRequestMsg': return 'Faction warfare join request';
            case 'FacWarCorpLeaveRequestMsg': return 'Faction warfare leave request';
            case 'FWCorpJoinMsg': return 'Corporation joined faction warfare';
            case 'FWCorpLeaveMsg': return 'Corporation left faction warfare';
            /*case 'GameTimeAdded': return '';
            case 'GameTimeReceived': return '';
            case 'IncursionCompletedMsg': return '';*/
            case 'InsuranceFirstShipMsg': return $notification->type;
            case 'InsurancePayoutMsg': return 'Insurance payout';
            /*case 'InsuranceExpirationMsg': return '';
            case 'InsuranceIssuedMsg': return '';
            case 'JumpCloneDeletedMsg1': return '';
            case 'JumpCloneDeletedMsg2': return '';*/
            case 'KillReportFinalBlow': return 'Kill report available (final blow)';
            case 'KillReportVictim': return 'Kill report available';
            case 'KillRightEarned': return 'Kill right earned';
            case 'MissionOfferExpirationMsg': return 'Mission expiring';
            /*case 'MissionTimeoutMsg': return '';
            case 'MoonminingAutomaticFracture': return '';
            case 'MoonminingExtractionFinished': return '';
            case 'MoonminingLaserFired': return '';*/
            case 'notificationTypeMoonminingExtractionStarted': return '';
            case 'NPCStandingsLost': return 'Standings changed';
            /*case 'OrbitalAttacked': return '';
            case 'OrbitalReinforced': return '';
            case 'OwnershipTransferred': return '';*/
            case 'SeasonalChallengeCompleted': return 'Seasonal challenge completed';
            /*case 'SovCommandNodeEventStarted': return '';
            case 'SovStationEnteredFreeport': return '';
            case 'SovStructureDestroyed': return '';
            case 'SovStructureReinforced': return '';
            case 'StationServiceDisabled': return '';
            case 'StationServiceEnabled': return '';*/
            case 'StoryLineMissionAvailableMsg': return 'Storyline mission available';
            case 'StructureAnchoring': return 'Structure anchoring';
            case 'StructureDestroyed': return 'Structure destroyed';
            /*case 'StructureItemsDelivered': return '';
            case 'StructureServicesOffline': return '';
            case 'StructureUnderAttack': return '';*/
            case 'StructureOnline': return 'Structure online';
            case 'TowerAlertMsg': return 'POS under attack';
            case 'TowerResourceAlertMsg': return 'POS runnig out of resourse';
            default: return $notification->type;
        }
    }

    /**
     * @param Notification $notification
     */
    public static function transformDescription(Notification $notification) {
        switch ($notification->type) {
            case 'AllianceCapitalChanged':
                preg_match('|allianceID:\s+(\d+)\s*solarSystemID:\s+(\d+)|s', $notification->text, $match);
                $alliance = \Yii::$app->evesso->allianceInfo($match[1]);
                $system = \Yii::$app->evesso->systemInfo($match[2]);
                return "Alliance: {$alliance['name']} {$alliance['ticker']}\nSystem: {$system['name']}";

            case 'AllWarDeclaredMsg':
                preg_match('|againstID:\s+(\d+).*declaredByID:\s+(\d+).*delayHours:\s+(\d+)|s', $notification->text, $match);
                $allianceFrom = \Yii::$app->evesso->allianceInfo($match[2]);
                $allianceTo = \Yii::$app->evesso->allianceInfo($match[1]);
                return "Declared by: {$allianceFrom['name']} {$allianceFrom['ticker']}\nDeclared to: {$allianceTo['name']} {$allianceTo['ticker']}\nDelay: {$match[3]}";

            case 'AllWarInvalidatedMsg':
                preg_match('|againstID:\s+(\d+).*declaredByID:\s+(\d+)|s', $notification->text, $match);
                $allianceFrom = \Yii::$app->evesso->allianceInfo($match[2]);
                $allianceTo = \Yii::$app->evesso->allianceInfo($match[1]);
                return "Declared by: {$allianceFrom['name']} {$allianceFrom['ticker']}\nDeclared to: {$allianceTo['name']} {$allianceTo['ticker']}";

            case 'AllWarRetractedMsg':
                preg_match('|againstID:\s+(\d+).*declaredByID:\s+(\d+)|s', $notification->text, $match);
                $allianceFrom = \Yii::$app->evesso->allianceInfo($match[2]);
                $allianceTo = \Yii::$app->evesso->allianceInfo($match[1]);
                return "Declared by: {$allianceFrom['name']} {$allianceFrom['ticker']}\nDeclared to: {$allianceTo['name']} {$allianceTo['ticker']}";

            case 'AllyJoinedWarAllyMsg':
                preg_match('|aggressorID:\s+(\d+).*allyID:\s+(\d+).*defenderID:\s+(\d+)|s', $notification->text, $match);
                $alliance = \Yii::$app->evesso->allianceInfo($match[2]);
                $allianceDefender = \Yii::$app->evesso->allianceInfo($match[3]);
                $allianceAgressor = \Yii::$app->evesso->allianceInfo($match[1]);
                return "Alliance: {$alliance['name']} {$alliance['ticker']}\nAgressor: {$allianceAgressor['name']} {$allianceAgressor['ticker']}\nDefender: {$allianceDefender['name']} {$allianceDefender['ticker']}";

            case 'BountyClaimMsg':
                preg_match('|amount:\s+(\d+)|i', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                return 'Amount: ' . number_format($match[1], 0, '.', ' ') . ' isk';

            case 'BountyPlacedChar':
                preg_match('|bounty:\s+([\d\.]+).*bountyPlacerID:\s+(\d+)|si', $notification->text, $match);
                $character = \Yii::$app->evesso->characterInfo($match[2]);
                \Yii::error($notification->type.': '.print_r($match, true));
                return 'Amount: ' . number_format($match[1], 0, '.', ' ') . " isk\nPlaced by: {$character['name']}";

            case 'CharAppAcceptMsg':
                preg_match('|applicationText:\s+(.*)\s*charID:\s+(\d+)\s*corpID:\s+(\d+)|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $corporation = \Yii::$app->evesso->corporationInfo($match[3]);
                $text = "{$corporation['name']} <{$corporation['ticker']}>";
                $applicationText = trim($match[1], " '\t\n\0\r");
                if(!empty($applicationText)) {
                    $text .= "\n{$applicationText}";
                }
                return $text;

            case 'CloneActivationMsg2':
                preg_match('|cloneStationID:\s+.*\s+(\d+)$|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $station = \Yii::$app->evesso->stationInfo($match[1]);
                return "Station: {$station['name']}";

            case 'FacWarCorpJoinRequestMsg':
                preg_match('|corpID:\s+(\d+)|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $corporation = \Yii::$app->evesso->corporationInfo($match[1]);
                return "{$corporation['name']} <{$corporation['ticker']}>";

            case 'FacWarCorpLeaveRequestMsg':
                preg_match('|corpID:\s+(\d+)|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $corporation = \Yii::$app->evesso->corporationInfo($match[1]);
                return "{$corporation['name']} <{$corporation['ticker']}>";

            case 'FWCorpJoinMsg':
                preg_match('|corpID:\s+(\d+)|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $corporation = \Yii::$app->evesso->corporationInfo($match[1]);
                return "{$corporation['name']} <{$corporation['ticker']}>";

            case 'FWCorpLeaveMsg':
                preg_match('|corpID:\s+(\d+)|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $corporation = \Yii::$app->evesso->corporationInfo($match[1]);
                return "{$corporation['name']} <{$corporation['ticker']}>";

            case 'InsuranceFirstShipMsg':
                preg_match('|shipTypeID:\s+(\d+)|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $ship = EveType::findOne($match[1]);
                return "Ship type: {$ship->title}";

            case 'InsurancePayoutMsg':
                preg_match('|amount:\s+(\d+)|i', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                return 'Amount: ' . number_format($match[1], 0, '.', ' ') . ' isk';

            case 'KillReportFinalBlow':
                preg_match('|killMailHash:\s+([\d\w]+)\s*killMailID:\s+(\d+).*victimShipTypeID:\s+(\d+)|s', $notification->text, $match);
                $killmail = \Yii::$app->evesso->killmail($match[1], $match[2]);
                $victim = \Yii::$app->evesso->characterInfo($killmail['victim']['character_id']);
                return "Victim: {$victim['name']}";

            case 'KillReportVictim':
                preg_match('|killMailHash:\s+(.*)\s*killMailID:\s+(\d+)\s*victimShipTypeID:\s+(\d+)|s', $notification->text, $match);
                $killmail = \Yii::$app->evesso->killmail($match[1], $match[2]);
                $victim = \Yii::$app->evesso->characterInfo($killmail['victim']['character_id']);
                return "Victim: {$victim['name']}";

            case 'KillRightEarned':
                preg_match('|charID:\s+(\d+)|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $character = \Yii::$app->evesso->characterInfo($match[1]);
                return $character['name'];

            case 'MissionOfferExpirationMsg':
                preg_match('|dungeonSolarSystemID:\s+(\d+)|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $system = \Yii::$app->evesso->systemInfo($match[1]);
                return "System: {$system['name']}";

            case 'NPCStandingsLost': return '';

            case 'SeasonalChallengeCompleted':
                preg_match('|points_awarded:\s+(\d+)|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                return "Points awarded: {$match[1]}";

            case 'StoryLineMissionAvailableMsg': return '';

            case 'StructureAnchoring':
            case 'StructureDestroyed':
                preg_match('|ownerCorpName:\s+([\s\d\w-_\+\.,]+)$\s*solarsystemID:\s+(\d+)|m', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $system = \Yii::$app->evesso->systemInfo($match[2]);
                return "System: {$system['name']}\nCorporation: {$match[1]}";

            case 'StructureOnline':
                preg_match('|solarsystemID:\s+(\d+).*structureTypeID:\s+(\d+)|s', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $system = \Yii::$app->evesso->systemInfo($match[1]);
                $type = \Yii::$app->evesso->typeInfo($match[2]);
                return "Structure: {$type['name']}\nSystem: {$system['name']}";

            case 'TowerAlertMsg':
                preg_match('|aggressorCorpID:\s+(\d+).*armorValue:\s+([\d\.]+).*hullValue:\s+([\d\.]+).*shieldValue:\s+([\d\.]+).*solarSystemID:\s+(\d+)|s', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $corporation = \Yii::$app->evesso->corporationInfo($match[1]);
                $system = \Yii::$app->evesso->systemInfo($match[5]);
                return "Agressor corporation: {$corporation['name']}\nSystem: {$system['name']}\nHitpoints (%): ".floor($match[4] * 100).' / '.floor($match[2] * 100).' / '.floor($match[3] * 100);

            case 'TowerResourceAlertMsg':
                preg_match('|solarSystemID:\s+(\d+).*typeID:\s+([\d\.]+).*typeID:\s+([\d\.]+)|s', $notification->text, $match);
                \Yii::error($notification->type.': '.print_r($match, true));
                $system = \Yii::$app->evesso->systemInfo($match[1]);
                $pos = \Yii::$app->evesso->typeInfo($match[2]);
                $resource = \Yii::$app->evesso->typeInfo($match[3]);
                return "Structure: {$pos['name']}\nSystem: {$system['name']}\nResourse required: {$resource['name']}";

            default: return $notification->text;
        }
    }

    /**
     * @param Notification $notification
     */
    public static function transformIcon(Notification $notification) {
        switch ($notification->sender_type) {
            case 'character':
                return \Yii::$app->evesso->getCharacterAvatar($notification->sender_id, 64);

            case 'corporation':
                if($notification->type === 'KillReportVictim') {
                    preg_match('|killMailHash:\s+(.*)\s*killMailID:\s+(\d+)\s*victimShipTypeID:\s+(\d+)|m', $notification->text, $match);
                    return \Yii::$app->evesso->getTypeIcon($match[3]);
                }
                else if($notification->type === 'KillReportFinalBlow') {
                    preg_match('|killMailHash:\s+([\d\w]+)\s*killMailID:\s+(\d+).*victimShipTypeID:\s+(\d+)|s', $notification->text, $match);
                    return \Yii::$app->evesso->getTypeIcon($match[3]);
                }
            case 'faction':
                return \Yii::$app->evesso->corporationIcon($notification->sender_id);

            default: return ArrayHelper::getValue(\Yii::$app->params, 'siteDomain', '') . '/images/eve/types/0_64.png';
        }
    }
}