<?php

namespace app\modules\comments;
use yii\base\BootstrapInterface;
use yii\web\Application;

/**
 * comments module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\comments\controllers';

    protected $urlRules = [
        'comments/<item_type:\w+>/<item_id:\d+>' => 'comments/comment/form',
        'comments/delete/<id:\d+>' => 'comments/comment/form',
    ];

    public function bootstrap($app)
    {
        if($app instanceof Application) {
            \Yii::$app->urlManager->addRules($this->urlRules);
        }
    }
}
