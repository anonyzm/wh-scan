<?php
use yii\widgets\Pjax;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
/** @var \yii\web\View $this */
/** @var \app\modules\comments\models\Comment[] $comments */

if(is_array($comments)) :
?>
    <section class="comment-list">
        <?php foreach($comments as $comment) : ?>
            <article class="row">
                <div class="col-md-2 col-sm-2 hidden-xs avatar-wrap">
                    <figure class="thumbnail">
                        <?= Html::img($comment->character->getAvatar(64), ['class' => 'img-responsive']) ?>
                        <!--figcaption class="text-center"><?= $comment->character->name ?></figcaption-->
                    </figure>
                </div>
                <div class="col-md-10 col-sm-10 comment-text-wrap">
                    <div class="panel panel-default arrow left">
                        <div class="panel-body">
                            <header class="text-left">
                                <div class="comment-user"><i class="fa fa-user"></i> <?= $comment->character->name ?></div>
                                <time class="comment-date grey-text" datetime="<?= date('d-m-Y H:i',$comment->created_at) ?>"><i class="fa fa-clock-o"></i> <?= date('M d, Y (H:i)',$comment->created_at) ?></time>
                            </header>
                            <div class="comment-post">
                                <p>
                                    <?= Html::encode($comment->text) ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        <?php endforeach; ?>
    </section>
<?php endif; ?>
<?php
$script =<<<JS
$('.comment-post p').each(function () {
    var comment = $(this).html();
    $(this).html(emojione.toImage(comment));
});
JS;
$this->registerJs($script);
?>