<?php
/* @var $this \yii\web\View */
?>
<div class="eve-sso-character">
    <span class="character-name"><?= \Yii::$app->evesso->characterName; ?></span>
    <?= \yii\bootstrap\Html::img(\Yii::$app->evesso->characterAvatar, ['class' => 'character-avatar']) ?>
</div>
