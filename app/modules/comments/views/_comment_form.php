<?php
use yii\widgets\Pjax;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var \app\modules\comments\models\Comment $newComment */
/** @var \app\modules\comments\models\Comment[] $comments */
/** @var integer $item_id */
/** @var string $item_type */
/** @var string $title */
?>
<?php Pjax::begin(['enablePushState' => false]); ?>
<div class="comment-form-wrap <?= \Yii::$app->evesso->isLoggedIn() ? 'logged-in' : 'logged-out' ?>">
    <?php if(!empty($title)) : ?><h5><?= $title ?></h5><?php endif ?>
    <?php
    $form = ActiveForm::begin([
        'action' => "/comments/{$item_type}/{$item_id}",
        'method' => 'post',
        'options' => ['data-pjax' => true],
    ]);
    echo Html::input('hidden', 'title', $title);

    $options = \Yii::$app->evesso->isLoggedIn() ? [] : ['disabled' => 'disabled'];
    echo \mervick\emojionearea\Widget::widget([
        'name' => 'Comment[text]',
        'pluginOptions' => [
            'saveEmojisAs' => 'shortname',
        ],
        'options' => $options,
    ]); ?>

    <div class="row">
        <div class="col-md-3 col-sm-3">
            <?= Html::submitButton('Submit', ArrayHelper::merge(['class' => 'comment-submit-button'], $options)) ?>
        </div>
        <div class="col-md-3 col-sm-6">
            <?php if(\Yii::$app->evesso->isAdmin()) : ?>
                <?= Html::a('Edit comments',['/backend/comment',
                    'CommentSearch[item_type]'=> $item_type,
                    'CommentSearch[item_id]'=> $item_id,
                ]) ?>
            <?php endif ?>
        </div>
        <div>
            <?php if (\Yii::$app->evesso->isLoggedIn()) : ?>
                <?= $this->render('_character_card'); ?>
            <?php else : ?>
                <?= Html::a('', '/sso/login', ['class' => 'eve-login-button']) ?>
            <?php endif; ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?= $this->render('_comment_list', [
    'comments' => $comments,
]); ?>
<?php Pjax::end(); ?>
