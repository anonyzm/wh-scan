<?php
namespace app\modules\comments\widgets;

use app\modules\comments\models\Comment;
use yii\bootstrap\Widget;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

class CommentWidget extends Widget
{
    public $item_id = null;
    public $item_type = null;
    public $title = '';

    public function run()
    {
        $newComment = new Comment();
        $newComment->item_type = $this->item_type;
        $newComment->item_id = $this->item_id;

        if(\Yii::$app->request->isPost) {
            $this->title = \Yii::$app->request->post('title');
            $params = ArrayHelper::merge(\Yii::$app->request->post(), [
                'Comment' => [
                    'character_id' => \Yii::$app->evesso->user->id,
                ]
            ]);

            if(!$newComment->load($params)) {
                throw new BadRequestHttpException('Error loading form data!');
            }

            if($newComment->validate()) {
                $newComment->save();
            }
        }

        $comments = Comment::find()->where(['item_id' => $this->item_id])
            ->andWhere(['item_type' => $this->item_type])
            ->joinWith('character')
            ->orderBy('`created_at` DESC')->all();

        return $this->render('@app/modules/comments/views/_comment_form', [
            'title' => $this->title,
            'item_id' => $this->item_id,
            'item_type' => $this->item_type,
            'newComment' => $newComment,
            'comments' => $comments,
        ]);
    }
}