<?php

namespace app\modules\comments\controllers;

use app\modules\comments\widgets\CommentWidget;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;

class CommentController extends Controller
{
    public function beforeAction($action)
    {
        if(!\Yii::$app->evesso->isLoggedIn()) {
            throw new UnauthorizedHttpException('Please log in using EVE SSO!');
        }

        return parent::beforeAction($action);
    }

    public function actionForm($item_type, $item_id)
    {
        return CommentWidget::widget([
            'item_id' => $item_id,
            'item_type' => $item_type,
        ]);
    }

}
