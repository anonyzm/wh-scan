<?php

namespace app\modules\comments\models;

use app\models\EveSsoUser;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%comment}}".
 *
 * @property integer $id
 * @property integer $item_id
 * @property string $item_type
 * @property integer $character_id
 * @property string $text
 * @property integer $created_at
 * @property EveSsoUser $character
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'character_id'], 'required'],
            [['item_id', 'character_id'], 'integer'],
            [['item_type'], 'string'],
            [['text'], 'string'],
        ];
    }

    public function getCharacter() {
        return $this->hasOne(EveSsoUser::className(), ['id' => 'character_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'item_type' => 'Item Type',
            'character_id' => 'Character ID',
            'text' => 'Text',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
}
