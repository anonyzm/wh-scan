#!/bin/bash
if [ ! -f "./app/.env" ]; then
  echo ".env not installed - run `init.sh` instead!"
else
  yes | cp "./app/.env.$1" ./app/.env
  echo ".env created"
fi

if [ ! -h "./.env" ]; then
  echo 'symlink not found'
else
  rm -rf ./.env
  echo "symlink deleted"
fi

ln -s ./app/.env ./.env
echo "symlink created"

docker-compose up -d --force-recreate
docker-compose exec --user application php composer -d/app update
if [ ! -h "./app/vendor/bower" ]; then
  docker-compose exec --user application php ln -s /app/vendor/bower-asset /app/vendor/bower
  echo "bower fixed created"
fi
docker-compose exec --user application php /app/yii app/init